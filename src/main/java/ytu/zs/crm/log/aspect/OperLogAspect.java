package ytu.zs.crm.log.aspect;


import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ytu.zs.crm.common.utils.APIUtil;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessStatus;
import ytu.zs.crm.log.pojo.SysOperLog;
import ytu.zs.crm.log.service.ISysOperLogService;
import ytu.zs.crm.log.utils.IpUtils;
import ytu.zs.crm.log.utils.ServletUtils;
import ytu.zs.crm.log.utils.StringUtil;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 操作日志记录处理
 */
@Aspect
@Slf4j
@Component
public class OperLogAspect {
    /**
     * sysOperLogService
     */
    @Autowired
    private ISysOperLogService sysOperLogService;

    /**
     * 配置织入点
     */
    @Pointcut("@annotation(ytu.zs.crm.log.annotation.OperLog)")
    public void logPointCut() {
    }

    /**
     * 处理完请求后执行
     *
     * @param joinPoint 切点
     */
    @AfterReturning(pointcut = "logPointCut()")
    public void doAfterReturning(JoinPoint joinPoint) {
        handleLog(joinPoint, null);
    }

    /**
     * 拦截异常操作
     *
     * @param joinPoint 切点
     * @param e         异常
     */
    @AfterThrowing(value = "logPointCut()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Exception e) {
        handleLog(joinPoint, e);
    }

    /**
     * handleLog
     *
     * @param joinPoint joinPoint
     * @param e         e
     */
    protected void handleLog(final JoinPoint joinPoint, final Exception e) {
        try {
            // 获得注解
            OperLog controllerLog = getAnnotationLog(joinPoint);
            if (controllerLog == null) {
                return;
            }
            // *========数据库日志=========*//
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SysOperLog operLog = new SysOperLog();
            operLog.setOperId(sdf.format(new Date()));
            operLog.setStatus(String.valueOf(BusinessStatus.SUCCESS.ordinal()));
            // 请求的地址
            HttpServletRequest request = ServletUtils.getRequest();
            String ip = IpUtils.getIpAddr(request);
            operLog.setOperIp(ip);
            operLog.setOperUrl(request.getRequestURI());
            operLog.setOperLocation(APIUtil.getCityByIP(ip));
            if (e != null) {
                operLog.setStatus(String.valueOf(BusinessStatus.FAIL.ordinal()));
                operLog.setErrorMsg(StringUtil.substring(e.getMessage(), 0, 2000));
            }
            // 设置方法名称
            String className = joinPoint.getTarget().getClass().getName();
            String methodName = joinPoint.getSignature().getName();
            operLog.setMethod(className + "." + methodName + "()");
            operLog.setOperName(request.getHeader("username"));
            // 设置请求方式
            operLog.setRequestMethod(request.getMethod());
            // 处理设置注解上的参数
            Object[] args = joinPoint.getArgs();
            getControllerMethodDescription(controllerLog, operLog, args);
            sysOperLogService.insertOperLog(operLog);
        } catch (Exception exp) {
            // 记录本地异常日志
            log.error("==前置通知异常==");
            log.error("异常信息:{}", exp.getMessage());
            exp.printStackTrace();
        }
    }

    /**
     * 获取注解中对方法的描述信息 用于Controller层注解
     *
     * @param log     日志
     * @param operLog 操作日志
     * @param args    args
     * @throws Exception Exception
     */
    public void getControllerMethodDescription(OperLog log, SysOperLog operLog, Object[] args) throws Exception {
        // 设置action动作
        operLog.setBusinessType(String.valueOf(log.businessType()));
        // 设置标题
        operLog.setTitle(log.title());
        // 设置操作人类别
        operLog.setOperatorType(String.valueOf(log.operatorType()));
        // 是否需要保存request，参数和值
        if (log.isSaveRequestData()) {
            // 获取参数的信息，传入到数据库中。
            setRequestValue(operLog, args);
        }
    }

    /**
     * 获取请求的参数，放到log中
     *
     *
     * @param operLog 操作日志
     * @param args    args
     * @throws Exception 异常
     */
    private void setRequestValue(SysOperLog operLog, Object[] args) throws Exception {
        List<?> param = new ArrayList<>(Arrays.asList(args)).stream().filter(p -> !(p instanceof ServletResponse))
                .collect(Collectors.toList());
        // 记录操作人
        if (param != null && !param.isEmpty()) {
            String jsonString = JSON.toJSONString(param.get(0), true);
            Map map = JSONObject.parseObject(jsonString, Map.class);
        }
        List paramJson = new ArrayList();
        for (Object p : param) {
            paramJson.add(JSONObject.toJavaObject(isExistField("password", p), p.getClass()));
        }
        String params = JSON.toJSONString(paramJson, true);
        operLog.setOperParam(StringUtil.substring(params, 0, 2000));
        if (param.contains("username")) {
            params = StringUtil.substring(params, params.indexOf("username") + 11, 2000);
            operLog.setOperName(StringUtil.substring(params, 0, params.indexOf('"')));
        }
    }

    /**
     * 是否存在注解，如果存在就获取
     *
     * @param joinPoint joinPoint
     * @return OperLog
     * @throws Exception Exception
     */
    private OperLog getAnnotationLog(JoinPoint joinPoint) throws Exception {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        if (method != null) {
            return method.getAnnotation(OperLog.class);
        }
        return null;
    }

    public static JSONObject isExistField(String field, Object obj) {
        if (obj == null || StringUtils.isEmpty(field)) {
            return null;
        }
        try{
            Object o = JSON.toJSON(obj);
            JSONObject jsonObj = new JSONObject();
            if (o instanceof JSONObject) {
                jsonObj = (JSONObject) o;
            }
            if (jsonObj.containsKey(field)) {
                jsonObj.put("password", "******");
            }
            return jsonObj;
        }catch (Exception e){
            return null;
        }

    }
}
