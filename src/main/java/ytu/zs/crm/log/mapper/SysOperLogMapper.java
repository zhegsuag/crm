package ytu.zs.crm.log.mapper;

import org.springframework.stereotype.Component;
import ytu.zs.crm.log.pojo.SysOperLog;

import java.util.List;

/**
 * 操作日志 数据层
 *
 * @author zhegsuag
 */
@Component
public interface SysOperLogMapper {
    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    void insertOperLog(SysOperLog operLog);

    /**
     * 查找操作日志
     *
     */
    List<SysOperLog> selectOperLog();

}
