package ytu.zs.crm.log.pojo;


import ytu.zs.crm.log.utils.BaseEntity;
import ytu.zs.crm.log.utils.Excel;
import java.util.Date;

public class SysOperLog extends BaseEntity {
    private static final long serialVersionUID = -5556121284445360558L;
    private String operId ;
    @Excel(
            name = "操作模块"
    )
    private String title ;
    @Excel(
            name = "业务类型",
            readConverterExp = "0=其它,1=新增,2=修改,3=删除,4=授权,5=导出,6=导入,7=强退,8=生成代码,9=清空数据"
    )
    private String businessType;
    @Excel(
            name = "请求方法"
    )
    private String method ;
    @Excel(
            name = "请求方式"
    )
    private String requestMethod ;
    @Excel(
            name = "操作类别",
            readConverterExp = "0=其它,1=后台用户,2=手机端用户"
    )
    private String operatorType;
    @Excel(
            name = "操作人员"
    )
    private String operName ;
    @Excel(
            name = "部门名称"
    )
    private String deptName ;
    @Excel(
            name = "请求地址"
    )
    private String operUrl ;
    @Excel(
            name = "操作地址"
    )
    private String operIp ;
    @Excel(
            name = "操作地点"
    )
    private String operLocation ;
    @Excel(
            name = "请求参数"
    )
    private String operParam ;
    @Excel(
            name = "状态",
            readConverterExp = "0=正常,1=异常"
    )
    private String status;
    @Excel(
            name = "错误消息"
    )
    private String errorMsg ;
    @Excel(
            name = "操作时间",
            width = 30.0D,
            dateFormat = "yyyy-MM-dd HH:mm:ss"
    )
    private Date operTime;


    public String getOperId() {
        return operId;
    }

    public void setOperId(String operId) {
        this.operId = operId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }

    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getOperUrl() {
        return operUrl;
    }

    public void setOperUrl(String operUrl) {
        this.operUrl = operUrl;
    }

    public String getOperIp() {
        return operIp;
    }

    public void setOperIp(String operIp) {
        this.operIp = operIp;
    }

    public String getOperLocation() {
        return operLocation;
    }

    public void setOperLocation(String operLocation) {
        this.operLocation = operLocation;
    }

    public String getOperParam() {
        return operParam;
    }

    public void setOperParam(String operParam) {
        this.operParam = operParam;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Date getOperTime() {
        return operTime;
    }

    public void setOperTime(Date operTime) {
        this.operTime = operTime;
    }

    public String toString() {
        return "SysOperLog(operId=" + this.getOperId() + ", title=" + this.getTitle() + ", businessType=" + this.getBusinessType() + ", method=" + this.getMethod() + ", requestMethod=" + this.getRequestMethod() + ", operatorType=" + this.getOperatorType() + ", operName=" + this.getOperName() + ", deptName=" + this.getDeptName() + ", operUrl=" + this.getOperUrl() + ", operIp=" + this.getOperIp() + ", operLocation=" + this.getOperLocation() + ", operParam=" + this.getOperParam() + ", status=" + this.getStatus() + ", errorMsg=" + this.getErrorMsg() + ", operTime=" + this.getOperTime() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof SysOperLog)) {
            return false;
        } else {
            SysOperLog other = (SysOperLog)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label191: {
                    Object this$operId = this.getOperId();
                    Object other$operId = other.getOperId();
                    if (this$operId == null) {
                        if (other$operId == null) {
                            break label191;
                        }
                    } else if (this$operId.equals(other$operId)) {
                        break label191;
                    }

                    return false;
                }

                Object this$title = this.getTitle();
                Object other$title = other.getTitle();
                if (this$title == null) {
                    if (other$title != null) {
                        return false;
                    }
                } else if (!this$title.equals(other$title)) {
                    return false;
                }

                Object this$businessType = this.getBusinessType();
                Object other$businessType = other.getBusinessType();
                if (this$businessType == null) {
                    if (other$businessType != null) {
                        return false;
                    }
                } else if (!this$businessType.equals(other$businessType)) {
                    return false;
                }

                label170: {
                    Object this$method = this.getMethod();
                    Object other$method = other.getMethod();
                    if (this$method == null) {
                        if (other$method == null) {
                            break label170;
                        }
                    } else if (this$method.equals(other$method)) {
                        break label170;
                    }

                    return false;
                }

                label163: {
                    Object this$requestMethod = this.getRequestMethod();
                    Object other$requestMethod = other.getRequestMethod();
                    if (this$requestMethod == null) {
                        if (other$requestMethod == null) {
                            break label163;
                        }
                    } else if (this$requestMethod.equals(other$requestMethod)) {
                        break label163;
                    }

                    return false;
                }

                Object this$operatorType = this.getOperatorType();
                Object other$operatorType = other.getOperatorType();
                if (this$operatorType == null) {
                    if (other$operatorType != null) {
                        return false;
                    }
                } else if (!this$operatorType.equals(other$operatorType)) {
                    return false;
                }

                Object this$operName = this.getOperName();
                Object other$operName = other.getOperName();
                if (this$operName == null) {
                    if (other$operName != null) {
                        return false;
                    }
                } else if (!this$operName.equals(other$operName)) {
                    return false;
                }

                label142: {
                    Object this$deptName = this.getDeptName();
                    Object other$deptName = other.getDeptName();
                    if (this$deptName == null) {
                        if (other$deptName == null) {
                            break label142;
                        }
                    } else if (this$deptName.equals(other$deptName)) {
                        break label142;
                    }

                    return false;
                }

                label135: {
                    Object this$operUrl = this.getOperUrl();
                    Object other$operUrl = other.getOperUrl();
                    if (this$operUrl == null) {
                        if (other$operUrl == null) {
                            break label135;
                        }
                    } else if (this$operUrl.equals(other$operUrl)) {
                        break label135;
                    }

                    return false;
                }

                Object this$operIp = this.getOperIp();
                Object other$operIp = other.getOperIp();
                if (this$operIp == null) {
                    if (other$operIp != null) {
                        return false;
                    }
                } else if (!this$operIp.equals(other$operIp)) {
                    return false;
                }

                label121: {
                    Object this$operLocation = this.getOperLocation();
                    Object other$operLocation = other.getOperLocation();
                    if (this$operLocation == null) {
                        if (other$operLocation == null) {
                            break label121;
                        }
                    } else if (this$operLocation.equals(other$operLocation)) {
                        break label121;
                    }

                    return false;
                }

                Object this$operParam = this.getOperParam();
                Object other$operParam = other.getOperParam();
                if (this$operParam == null) {
                    if (other$operParam != null) {
                        return false;
                    }
                } else if (!this$operParam.equals(other$operParam)) {
                    return false;
                }

                label107: {
                    Object this$status = this.getStatus();
                    Object other$status = other.getStatus();
                    if (this$status == null) {
                        if (other$status == null) {
                            break label107;
                        }
                    } else if (this$status.equals(other$status)) {
                        break label107;
                    }

                    return false;
                }

                Object this$errorMsg = this.getErrorMsg();
                Object other$errorMsg = other.getErrorMsg();
                if (this$errorMsg == null) {
                    if (other$errorMsg != null) {
                        return false;
                    }
                } else if (!this$errorMsg.equals(other$errorMsg)) {
                    return false;
                }

                Object this$operTime = this.getOperTime();
                Object other$operTime = other.getOperTime();
                if (this$operTime == null) {
                    if (other$operTime != null) {
                        return false;
                    }
                } else if (!this$operTime.equals(other$operTime)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof SysOperLog;
    }

    public int hashCode() {
        boolean PRIME = true;
        int result = 1;
        Object $operId = this.getOperId();
        result = result * 59 + ($operId == null ? 43 : $operId.hashCode());
        Object $title = this.getTitle();
        result = result * 59 + ($title == null ? 43 : $title.hashCode());
        Object $businessType = this.getBusinessType();
        result = result * 59 + ($businessType == null ? 43 : $businessType.hashCode());
        Object $method = this.getMethod();
        result = result * 59 + ($method == null ? 43 : $method.hashCode());
        Object $requestMethod = this.getRequestMethod();
        result = result * 59 + ($requestMethod == null ? 43 : $requestMethod.hashCode());
        Object $operatorType = this.getOperatorType();
        result = result * 59 + ($operatorType == null ? 43 : $operatorType.hashCode());
        Object $operName = this.getOperName();
        result = result * 59 + ($operName == null ? 43 : $operName.hashCode());
        Object $deptName = this.getDeptName();
        result = result * 59 + ($deptName == null ? 43 : $deptName.hashCode());
        Object $operUrl = this.getOperUrl();
        result = result * 59 + ($operUrl == null ? 43 : $operUrl.hashCode());
        Object $operIp = this.getOperIp();
        result = result * 59 + ($operIp == null ? 43 : $operIp.hashCode());
        Object $operLocation = this.getOperLocation();
        result = result * 59 + ($operLocation == null ? 43 : $operLocation.hashCode());
        Object $operParam = this.getOperParam();
        result = result * 59 + ($operParam == null ? 43 : $operParam.hashCode());
        Object $status = this.getStatus();
        result = result * 59 + ($status == null ? 43 : $status.hashCode());
        Object $errorMsg = this.getErrorMsg();
        result = result * 59 + ($errorMsg == null ? 43 : $errorMsg.hashCode());
        Object $operTime = this.getOperTime();
        result = result * 59 + ($operTime == null ? 43 : $operTime.hashCode());
        return result;
    }
}
