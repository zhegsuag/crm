package ytu.zs.crm.log.utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Entity基类
 *
 * @author zhegsuag
 */
@Getter
@Setter
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 搜索值
     */
    private String searchValue;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 排序字段
     */
    private String sortField;

    /**
     * 排序方式
     */
    private String sortOrder;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 备注
     */
    private String remark;
    /**
     * beginTime
     */
    private String beginTime;
    /**
     * endTime
     */
    private String endTime;
    /**
     * 请求参数
     */
    private Map<String, Object> params;

    /**
     * getParams
     *
     * @return Map
     */
    public Map<String, Object> getParams() {
        if (params == null) {
            params = new HashMap<>();
        }
        return params;
    }

    /**
     * setParams
     *
     * @param params params
     */
    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    /**
     * getCreateTime
     *
     * @return Date
     */
    public Date getCreateTime() {
        if (createTime != null) {
            return new Date(this.createTime.getTime());
        } else {
            return null;
        }
    }

    /**
     * setCreateTime
     *
     * @param createTime createTime
     */
    public void setCreateTime(Date createTime) {
        if (createTime != null) {
            this.createTime = (Date) createTime.clone();
        } else {
            this.createTime = null;
        }
    }

    /**
     * getUpdateTime
     *
     * @return Date
     */
    public Date getUpdateTime() {
        if (updateTime != null) {
            return new Date(this.updateTime.getTime());
        } else {
            return null;
        }
    }

    /**
     * setUpdateTime
     *
     * @param updateTime updateTime
     */
    public void setUpdateTime(Date updateTime) {
        if (updateTime != null) {
            this.updateTime = (Date) updateTime.clone();
        } else {
            this.updateTime = null;
        }
    }
}
