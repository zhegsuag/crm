package ytu.zs.crm.log.annotation;



import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.log.enums.OperatorType;

import java.lang.annotation.*;

/**
 * 自定义操作日志记录注解
 *
 * @author zhegsuag
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperLog {
    /**
     * 模块
     * @return String
     */
    String title() default "";

    /**
     * 功能
     * @return BusinessType
     */
    BusinessType businessType() default BusinessType.SELECT;

    /**
     * 操作人类别
     * @return OperatorType
     */
    OperatorType operatorType() default OperatorType.MANAGE;

    /**
     * 是否保存请求的参数
     * @return boolean
     */
    boolean isSaveRequestData() default true;
}
