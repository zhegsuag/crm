package ytu.zs.crm.log.enums;

/**
 * 操作状态
 *
 * @author zhegsuag
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
