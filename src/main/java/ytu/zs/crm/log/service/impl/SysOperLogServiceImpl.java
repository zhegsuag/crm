package ytu.zs.crm.log.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ytu.zs.crm.log.mapper.SysOperLogMapper;
import ytu.zs.crm.log.pojo.SysOperLog;
import ytu.zs.crm.log.service.ISysOperLogService;

/**
 * 操作日志 服务层处理
 *
 * @author zhegsuag
 */
@Service
public class SysOperLogServiceImpl implements ISysOperLogService {
    /**
     * SysOperLogMapper
     */
    @Autowired
    private SysOperLogMapper operLogMapper;

    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    @Override
    public void insertOperLog(SysOperLog operLog) {
        operLogMapper.insertOperLog(operLog);
    }

}
