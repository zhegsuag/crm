package ytu.zs.crm.log.service;


import ytu.zs.crm.log.pojo.SysOperLog;

/**
 * 操作日志 服务层
 *
 * @author zhegsuag
 */
public interface ISysOperLogService {
    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    public void insertOperLog(SysOperLog operLog);

}
