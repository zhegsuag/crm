package ytu.zs.crm.scheduled;

import ytu.zs.crm.mapper.StateMapper;
import ytu.zs.crm.pojo.State;
import ytu.zs.crm.pojo.StateExample;
import ytu.zs.crm.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClassName: SendMail
 * @Author: zhegsuag
 **/

@Component
public class SendMail {

    @Autowired
    private MailService mailService;

    @Autowired
    private StateMapper stateMapper;

    @Scheduled(cron = "0 0 8 * * ?")
    public void sendMail() {
        StateExample example = new StateExample();
        example.createCriteria().andStateBetween("危险状态", "脱离状态");
        List<State> lists = stateMapper.selectByExample(example);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<style type=\"text/css\"> table.gridtable {\n" +
                "    font-family: verdana, arial, sans-serif;\n" +
                "    font-size: 11px;\n" +
                "    color: #333333;\n" +
                "    border-width: 1px;\n" +
                "    border-color: #666666;\n" +
                "    border-collapse: collapse;\n" +
                "}\n" +
                "\n" +
                "table.gridtable th {\n" +
                "    border-width: 1px;\n" +
                "    padding: 8px;\n" +
                "    border-style: solid;\n" +
                "    border-color: #666666;\n" +
                "    background-color: #dedede;\n" +
                "}\n" +
                "\n" +
                "table.gridtable td {\n" +
                "    border-width: 1px;\n" +
                "    padding: 8px;\n" +
                "    border-style: solid;\n" +
                "    border-color: #666666;\n" +
                "    background-color: #ffffff;\n" +
                "} </style>\n" +
                "<div align=\"center\">\n" +
                "    <table class=\"gridtable\">\n" +
                "        <tr>\n" +
                "            <th>客户编号</th>\n" +
                "            <th>客户姓名</th>\n" +
                "            <th>订单数量</th>\n" +
                "            <th>当前状态</th>\n" +
                "        </tr>\n");
        lists.stream().forEach(list -> stringBuffer.append("<tr>")
                .append("<td>").append(list.getId()).append("</td>")
                .append("<td>").append(list.getName()).append("</td>")
                .append("<td>").append(list.getOrdnum()).append("</td>")
                .append("<td>").append(list.getState()).append("</td>")
                .append("</tr>"));
        stringBuffer.append("</table>\n" +
                "</div>");
        mailService.sendHtmlMail("CRM客户预警", String.valueOf(stringBuffer), new String[]{"zs011221@163.com"});
    }
}
