package ytu.zs.crm.scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ytu.zs.crm.common.utils.Epidemic;
import ytu.zs.crm.mapper.MessageMapper;
import ytu.zs.crm.pojo.Message;
import ytu.zs.crm.service.MailService;
import ytu.zs.crm.service.MessageService;
import ytu.zs.crm.service.impl.MessageServiceImpl;

import java.sql.SQLException;
import java.util.Date;

/**
 * @ClassName: SendMessage
 * @Author: zhegsuag
 * @Version: 1.0
 **/
@Component
public class SendMessage {

    @Autowired
    private MessageService messageService;

    @Scheduled(cron = "0 0 8 * * ?")
    public void sendMessage() {
        messageService.sendMessages();
    }

}
