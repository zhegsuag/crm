package ytu.zs.crm.service;

import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.pojo.StateInfo;

import java.util.List;

public interface StateInfoService {

    MsgInter<?> getStateInfo();

    MsgInter<?> get(List<StateInfo> list);
}
