package ytu.zs.crm.service;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.log.pojo.SysOperLog;

import java.io.IOException;
import java.util.List;

public interface LogService {

    PageInfo<?> selectOperLog(PageInfo<?> pageInfo);

    void excel();
}
