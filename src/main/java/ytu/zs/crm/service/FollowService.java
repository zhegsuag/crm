package ytu.zs.crm.service;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.pojo.Echart;
import ytu.zs.crm.pojo.Follow;
import ytu.zs.crm.pojo.LogOfFollow;

import java.util.Date;
import java.util.List;

public interface FollowService {

    PageInfo<Follow> getFollowPageInfo(PageInfo<?> pageInfo, String username);

    List<LogOfFollow> getHistory(Integer cid);

    int update(Follow follow);

    MsgInter<?> delete(Follow follow);

    List<Follow> backlog(String username, Date date);

    List<Echart> Echart2();

}
