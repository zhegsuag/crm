package ytu.zs.crm.service;

import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.texts.vo.TextsVO;
import ytu.zs.crm.pojo.Texts;

/**
 * @description: 待办事项
 * @author: zhegsuag
 */
public interface TextsService {
    MsgInter<?> getInfo();

    MsgInter<?> addText(Texts texts);

    MsgInter<?> update(TextsVO textsVO);
}
