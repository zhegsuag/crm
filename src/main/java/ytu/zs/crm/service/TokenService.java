package ytu.zs.crm.service;

import ytu.zs.crm.pojo.Account;

public interface TokenService {

    public String getToken(Account account);
}
