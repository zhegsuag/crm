package ytu.zs.crm.service;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.pojo.Orders;

/**
 * @description: 订单详情
 * @author: zhegsuag
 */
public interface OrdersService {

    /**
     *
     * @return 返回订单列表
     */
    MsgInter<?> getOrdersInfo();

    /**
     *
     * @param pageInfo 分页信息
     * @return 返回分页列表
     */
    PageInfo<?> getPaging(PageInfo<?> pageInfo);

    /**
     *
     * @param orders 订单信息
     * @return 返回添加成功
     */
    MsgInter<?> addOrders(Orders orders);

    /**
     *
     * @param id 订单id
     * @return 返回删除成功
     */
    MsgInter<?> deleteById(Integer id);

    /**
     *
     * @param orders 订单信息
     * @return 返回修改成功
     */
    MsgInter<?> update(Orders orders);
}
