package ytu.zs.crm.service;

import ytu.zs.crm.common.msg.MsgInter;

/**
 * @description: 客户状态
 * @author: zhegsuag
 */
public interface StateService {
    MsgInter<?> getStateInfo(String userName);

    MsgInter<?> getCustInfo();
}
