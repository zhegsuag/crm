package ytu.zs.crm.service;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.pojo.Company;

import java.util.List;

/**
 * @description: 公司信息
 * @author: zhegsuag
 */
public interface CompanyService {
    /**
     * 获取所有公司
     *
     * @return 返回list集合
     */
    List<Company> getAllList();

    /**
     *
     * @param pageInfo 分页信息
     * @return 返回分页列表
     */
    PageInfo<?> getPageInfo(PageInfo<?> pageInfo);

    /**
     *
     * @param company 公司信息
     * @return 返回添加成功
     */
    MsgInter<?> addCompany(Company company);

    /**
     *
     * @param company 公司信息
     * @return 返回修改成功
     */
    MsgInter<?> update(Company company);

    /**
     *
     * @param id 公司id
     * @return 返回删除成功
     */
    MsgInter<?> delete(Integer id);
}
