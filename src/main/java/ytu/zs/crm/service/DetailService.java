package ytu.zs.crm.service;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.orders.vo.DetailVO;

import java.util.List;

/**
 * @description: 订单明细
 * @author: zhegsuag
 */
public interface DetailService {

    List<DetailVO> getAllInfo();

    PageInfo<?> getPaging(PageInfo<?> pageInfo);

    MsgInter<?> handleState(DetailVO detailVO,Integer i);

    List<Integer> getNumOfOrder();
}
