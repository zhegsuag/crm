package ytu.zs.crm.service;


import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.pojo.Message;

import java.util.List;

public interface MessageService {

    void sendMessages();

    List<Message> pading(Integer state);

    MsgInter<?> sync(Integer mid, Integer state);
}
