package ytu.zs.crm.service;

import ytu.zs.crm.common.msg.MsgInter;

/**
 * @description: 最新消息
 * @author: zhegsuag
 */
public interface RecordService {

    MsgInter<?> getInfo();
}
