package ytu.zs.crm.service;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.pojo.Account;

import java.util.List;

/**
 * @description: 账户信息
 * @author: zhegsuag
 */
public interface AccountService {

    /**
     * 获取账户列表
     * @return 返回列表
     */
    List<Account> getAllList();

    PageInfo<?> paging(PageInfo<?> pageInfo);

    MsgInter<?> deleteById(Integer id);

    MsgInter<?> update(Account account);

    MsgInter<?> addAccount(Account account);
}
