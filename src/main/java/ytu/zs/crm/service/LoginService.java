package ytu.zs.crm.service;

import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.pojo.Account;

/**
 * @Description:
 * @author: zhegsuag
 */
public interface LoginService {

    /**
     * 登陆验证
     * @param username 用户名
     * @param password 密码
     * @return 返回状态
     */
    MsgAction<String> login(String username, String password);

    /**
     * 登陆验证
     * @param username 用户名
     * @param password 密码
     * @return 返回状态
     */
    MsgAction<Account> persion(String username, String password);

    /**
     * 登陆验证
     * @param account 用户
     * @return 返回状态
     */
    MsgAction<Account> updatepersion(Account account);

    /**
     * 信息重复验证
     * @param account 用户
     * @return 返回状态
     */
    MsgAction<Integer> selectinformation(Account account);

    Account selectofname(String name);

}
