package ytu.zs.crm.service.impl;

import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.mapper.RecordMapper;
import ytu.zs.crm.pojo.Record;
import ytu.zs.crm.pojo.RecordExample;
import ytu.zs.crm.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * @description: 最新消息
 * @author: zhegsuag
 */
@Service
public class RecordServiceImpl implements RecordService {

    @Autowired
    private RecordMapper recordMapper;

    @Override
    public MsgInter<?> getInfo() {
        List<Record> records = recordMapper.selectByExample(new RecordExample());
        Assert.notNull(records, "查询结果为空");
        //只显示当天的消息
        long current = System.currentTimeMillis();//当前时间毫秒数
        long zero = current / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();//今天零点零分零秒的毫秒数
        long twelve = zero + 24 * 60 * 60 * 1000 - 1;//今天23点59分59秒的毫秒数

        List<Record> recordList = new ArrayList<>();
        for (int i = 0; i < records.size(); i++) {
            long time = records.get(i).getCreatetime().getTime();

            if (time < twelve && time > zero) {
                recordList.add(records.get(i));
            }
        }
        return MsgAction.ok(recordList);
    }
}
