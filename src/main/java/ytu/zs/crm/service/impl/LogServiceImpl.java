package ytu.zs.crm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestContextHolder;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.company.vo.CompanyVO;
import ytu.zs.crm.controller.company.vo.CompanyVOConvert;
import ytu.zs.crm.log.mapper.SysOperLogMapper;
import ytu.zs.crm.log.pojo.SysOperLog;
import ytu.zs.crm.log.utils.ServletUtils;
import ytu.zs.crm.log.utils.StringUtil;
import ytu.zs.crm.pojo.Company;
import ytu.zs.crm.pojo.CompanyExample;
import ytu.zs.crm.service.LogService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @ClassName: LogServiceImpl
 * @Description: TODO
 * @Author: zhegsuag
 **/
@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private SysOperLogMapper sysOperLogMapper;

    @Override
    public PageInfo<?> selectOperLog(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息为空");
        PageHelper.startPage(
                ObjectUtils.defaultIfNull(pageInfo.getPageNum(), 0),
                ObjectUtils.defaultIfNull(pageInfo.getPageSize(), 10));
        List<SysOperLog> logs = sysOperLogMapper.selectOperLog();
        PageInfo<SysOperLog> logPageInfo = new PageInfo<>(logs);
        return logPageInfo;
    }

    @Override
    public void excel(){
        try {
            List<SysOperLog> logs = sysOperLogMapper.selectOperLog();
            // 读取项目根目录中的模板
            InputStream in = this.getClass().getResourceAsStream("/templates/crm-log.xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(in);
            // 只解析第一张 sheet 工作表
            XSSFSheet sheet = workbook.getSheetAt(0);
            XSSFCellStyle cellStyle = workbook.createCellStyle();
            addRow(logs, sheet, cellStyle);
            HttpServletResponse response = ServletUtils.getResponse();
            response.setCharacterEncoding("utf-8");
            // 下载使用"application/octet-stream"更标准
            response.setContentType("application/octet-stream;charset=UTF-8");
            response.setHeader("content-disposition", "attachment;filename=" + java.net.URLEncoder.encode("TC11EPBOM对比报表", "UTF-8") + ".xlsx");
            //workbook将Excel写入到response的输出流中，供页面下载
            workbook.write(response.getOutputStream());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void addRow(List<SysOperLog> logs, XSSFSheet sheet, XSSFCellStyle cellStyle) {
        int rowIndex = 2;
        for (SysOperLog log : logs) {
            Row row = sheet.createRow(rowIndex);
            row.createCell(0).setCellValue(rowIndex - 1);
            row.createCell(1).setCellValue(log.getOperId());
            row.createCell(2).setCellValue(log.getTitle());
            row.createCell(3).setCellValue(log.getBusinessType());
            row.createCell(4).setCellValue(log.getMethod());
            row.createCell(5).setCellValue(log.getRequestMethod());
            row.createCell(6).setCellValue(log.getOperName());
            row.createCell(7).setCellValue(log.getOperUrl());
            row.createCell(8).setCellValue(log.getOperId());
            row.createCell(9).setCellValue(log.getOperLocation());
            row.createCell(10).setCellValue(String.valueOf(log.getParams()));
            row.createCell(11).setCellValue(log.getErrorMsg());
            rowIndex++;

        }
    }
}
