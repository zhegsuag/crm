package ytu.zs.crm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.orders.dto.DetailDTO;
import ytu.zs.crm.controller.orders.vo.DetailVO;
import ytu.zs.crm.controller.orders.vo.DetailVOConvert;
import ytu.zs.crm.mapper.DetailMapper;
import ytu.zs.crm.mapper.TextsMapper;
import ytu.zs.crm.pojo.Detail;
import ytu.zs.crm.pojo.DetailExample;
import ytu.zs.crm.pojo.Texts;
import ytu.zs.crm.pojo.TextsExample;
import ytu.zs.crm.service.DetailService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @description: 订单明细
 * @author: zhegsuag
 */
@Service
public class DetailServiceImpl implements DetailService {

    @Autowired
    private DetailMapper detailMapper;
    @Autowired
    private TextsMapper  textsMapper;

    @Override
    public List<DetailVO> getAllInfo() {
        List<DetailDTO> info = detailMapper.getAllInfo();
        List<DetailVO> detailVOS = new ArrayList<>();
        for (int i = 0; i < info.size(); i++) {
            DetailVO detailVO = DetailVOConvert.INSTANCE.toVO(info.get(i));
            if (info.get(i).getState() == 0) {
                detailVO.setState("未处理");
            } else {
                detailVO.setState("已处理");
            }
            detailVOS.add(detailVO);
        }
        return detailVOS;
    }

    @Override
    public PageInfo<?> getPaging(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息为空");
        PageHelper.startPage(
                ObjectUtils.defaultIfNull(pageInfo.getPageNum(), 1),
                ObjectUtils.defaultIfNull(pageInfo.getPageSize(), 5));
        List<DetailDTO> allInfo = detailMapper.getAllInfo();
        allInfo.stream().forEach(item -> {
            item.getWid().setwName(item.getWid().getwName()+ "等商品") ;
        });
        PageInfo<DetailDTO> detailDTOPageInfo = new PageInfo<>(allInfo);

        List<DetailVO> detailVOS = new ArrayList<>();
        for (int i = 0; i < detailDTOPageInfo.getList().size(); i++) {
            DetailVO detailVO = DetailVOConvert.INSTANCE.toVO(detailDTOPageInfo.getList().get(i));
            if (detailDTOPageInfo.getList().get(i).getState() == 0) {
                detailVO.setState("未处理");
            } else {
                detailVO.setState("已处理");
            }
            detailVOS.add(detailVO);
        }

        PageInfo<DetailVO> info = new PageInfo<>();

        BeanUtils.copyProperties(detailDTOPageInfo, info);
        info.setList(detailVOS);

        return info;
    }

    @Transactional
    @Override
    public MsgInter<?> handleState(DetailVO detailVO, Integer i) {
        /**
         * i：1表示前端发送订单处理请求，0 表示发送撤销处理请求
         */
        //i=0，执行撤销处理请求
        if (i == 0) {
            String state = detailVO.getState();
            //判断订单状态是否为：未处理状态
            if ("未处理".equals(state)) {
                return MsgAction.getError("订单还未处理!请尽快处理!");
            }
            //订单已处理,撤销处理
            Detail detail = new Detail();
            detail.setState((byte) 0);
            detail.setWid(detailVO.getWid().getWid());
            detail.setOid(detailVO.getOid().getOid());
            detail.setCreatetime(detailVO.getCreateTime());
            detail.setHandletime(null);
            //根据oid修改该订单的状态
            DetailExample example = new DetailExample();
            example.createCriteria().andOidEqualTo(detailVO.getOid().getOid());

            int update = detailMapper.updateByExample(detail, example);
            //行数为0或小于0，撤销失败,否则撤销成功
            if (update <= 0) {
                return MsgAction.getError("撤销处理失败!");
            }
            //撤销后，将待办时间激活为：false，表示未处理
            TextsExample example1 = new TextsExample();
            example1.createCriteria().andOidEqualTo(detailVO.getOid().getOid());
            List<Texts> texts = textsMapper.selectByExample(example1);
            //通过上面订单编号查询该待办事项，判断是否存在
            if (texts.size() == 0) {
                //不存在该事项，则新建一个新事项，设置事项属性值
                Texts texts1 = new Texts();
                texts1.setOid(detailVO.getOid().getOid());
                texts1.setText("有一个编号为：" + detailVO.getOid().getOid() + "的订单未处理噢!!!赶紧去看看吧~");
                texts1.setFinish((byte) 0);
                //将新事项添加到表中
                textsMapper.insert(texts1);
                return MsgAction.getSuccess("撤销成功!");
            }
            //如果事项存在，则判断该事项是否为完成状态，1为完成状态，0为未完成状态
            if (texts.get(0).getFinish() == 1) {
                //如果完成状态，则将该事项的状态改为0，未完成状态
                texts.get(0).setFinish((byte) 0);
                //重新将该事项更新到表中
                textsMapper.updateByPrimaryKey(texts.get(0));
            }
            return MsgAction.getSuccess("撤销成功!");
        }

        //i=1，执行订单处理操作
        String state = detailVO.getState();
        //判断订单是否为：已处理状态
        if ("已处理".equals(state)) {
            return MsgAction.getError("订单已处理!请勿重复操作!");
        }
        //订单未处理，执行订单处理操作
        Detail detail = new Detail();
        detail.setState((byte) 1);
        detail.setWid(detailVO.getWid().getWid());
        detail.setOid(detailVO.getOid().getOid());
        detail.setCreatetime(detailVO.getCreateTime());
        detail.setHandletime(new Date());
        //根据oid修改该订单状态
        DetailExample example = new DetailExample();
        example.createCriteria().andOidEqualTo(detailVO.getOid().getOid());
        int update = detailMapper.updateByExample(detail, example);
        //行数为0或小于0，订单处理失败,否则处理成功
        if (update <= 0) {
            return MsgAction.getError("订单处理失败!");
        }

        //订单处理后，将待办事项设置为：true，表示已处理
        TextsExample example1 = new TextsExample();
        example1.createCriteria().andOidEqualTo(detailVO.getOid().getOid());
        List<Texts> texts = textsMapper.selectByExample(example1);
        //通过上面订单编号查询该待办事项，判断是否存在
        if (texts == null) {
            //不存在该事项，则新建一个新事项，设置事项属性值
            Texts texts1 = new Texts();
            texts1.setOid(detailVO.getOid().getOid());
            texts1.setText("有一个编号为：" + detailVO.getOid().getOid() + "的订单未处理噢!!!赶紧去看看吧~");
            texts1.setFinish((byte) 1);
            //将新事项添加到表中
            textsMapper.insert(texts1);
            return MsgAction.getSuccess("订单处理成功!");
        }
        //如果事项存在，则判断该事项是否为完成状态，1为完成状态，0为未完成状态
        if (texts.get(0).getFinish() == 0) {
            //如果是未完成状态，则将该事项的状态改为1，完成状态
            texts.get(0).setFinish((byte) 1);
            //重新将该事项更新到表中
            textsMapper.updateByPrimaryKey(texts.get(0));
        }

        return MsgAction.getSuccess("订单处理成功!");
    }

    @Override
    public List<Integer> getNumOfOrder() {
        List<Integer> list = new ArrayList();
        list.add(detailMapper.getOrdersNum1());
        list.add(detailMapper.getOrdersNum2());
        list.add(detailMapper.getOrdersNum3());
        list.add(detailMapper.getOrdersNum4());
        list.add(detailMapper.getOrdersNum5());
        list.add(detailMapper.getOrdersNum6());
        list.add( detailMapper.getOrdersNum7());
        return list;
    }

}
