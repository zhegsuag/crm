package ytu.zs.crm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.mapper.AccountMapper;
import ytu.zs.crm.mapper.FollowMapper;
import ytu.zs.crm.mapper.LogOfFollowMapper;
import ytu.zs.crm.pojo.Echart;
import ytu.zs.crm.pojo.Follow;
import ytu.zs.crm.pojo.LogOfFollow;
import ytu.zs.crm.service.FollowService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: FollowServiceImpl
 * @Author: zhegsuag
 * @Version: 1.0
 **/

@Service
public class FollowServiceImpl implements FollowService {

    @Autowired
    private FollowMapper followMapper;

    @Autowired
    private LogOfFollowMapper logOfFollowMapper;

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public PageInfo<Follow> getFollowPageInfo(PageInfo<?> pageInfo, String username) {
        Assert.notNull(pageInfo, "分页信息为空");
        PageHelper.startPage(
                ObjectUtils.defaultIfNull(pageInfo.getPageNum(), 0),
                ObjectUtils.defaultIfNull(pageInfo.getPageSize(), 10));
        List<Follow> follows = followMapper.selectByPading(username);
        PageInfo<Follow> PageInfo = new PageInfo<>(follows);
        return PageInfo;
    }

    @Override
    public List<LogOfFollow> getHistory(Integer cid) {
        Assert.notNull(cid, "客户id未选中");
        return logOfFollowMapper.selectByCid(cid);
    }

    @Override
    public int update(Follow follow) {
        Integer logs = logOfFollowMapper.insertSelective(follow);
        Assert.notNull(logs, "数据异常");
        return followMapper.updateByPrimaryKey(follow);
    }

    @Override
    public MsgInter<?> delete(Follow follow) {
        Assert.notNull(follow.getLfId(), "数据异常");
        Integer follows = logOfFollowMapper.deleteByPrimaryKey(follow.getLfId());
        if (follows != 1) {
            return MsgAction.error("删除失败");
        }
        return MsgAction.SUCCESS;
    }

    @Override
    public List<Follow> backlog(String username, Date date) {
        List<Follow> follows = followMapper.selectBacklog(username, date);
        return follows;
    }

    @Override
    public List<Echart> Echart2() {
        List<Echart> echarts = new ArrayList<>();
        List<String> names = accountMapper.selectname();
        names.stream().forEach(n -> {
            echarts.add(new Echart(logOfFollowMapper.selectByUserName(n),n));
        });
        return echarts;
    }
}
