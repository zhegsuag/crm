package ytu.zs.crm.service.impl;

import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.cust.dto.StateDTO;
import ytu.zs.crm.controller.cust.vo.StateVO;
import ytu.zs.crm.controller.cust.vo.StateVOConvert;
import ytu.zs.crm.mapper.StateMapper;
import ytu.zs.crm.pojo.State;
import ytu.zs.crm.pojo.StateExample;
import ytu.zs.crm.service.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 客户状态
 * @author: zhegsuag
 */
@Service
public class StateServiceImpl implements StateService {

    @Autowired
    private StateMapper stateMapper;


    @Override
    public MsgInter<?> getStateInfo(String userName) {
        List<StateDTO> stateDTOS = stateMapper.select(userName);
        Assert.notNull(stateDTOS,"查询为空!");
        List<StateVO> stateVOS = StateVOConvert.INSTANCE.toVO(stateDTOS);
        List<List<StateVO>> users = new ArrayList<>();
        users.add(0,stateVOS);
        return MsgAction.ok(users);
    }

    @Override
    public MsgInter<?> getCustInfo() {
        StateExample example = new StateExample();
        example.createCriteria().andStateBetween("危险状态","脱离状态");
        List<State> states = stateMapper.selectByExample(example);
        Assert.notNull(states,"查询为空!");
        return MsgAction.ok(states);
    }
}
