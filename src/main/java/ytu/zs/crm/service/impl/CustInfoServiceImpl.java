package ytu.zs.crm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.cust.vo.CustomerVO;
import ytu.zs.crm.controller.cust.vo.CustomerVOConvert;
import ytu.zs.crm.mapper.*;
import ytu.zs.crm.pojo.*;
import ytu.zs.crm.service.CustInfoService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @author: zhegsuag
 */
@Service
public class CustInfoServiceImpl implements CustInfoService {

    @Autowired
    private CustomerMapper customerMapper;
    @Autowired
    private StateMapper    stateMapper;
    @Autowired
    private RecordMapper   recordMapper;
    @Autowired
    private OrdersMapper   ordersMapper;
    @Autowired
    private FollowMapper followMapper;
    @Autowired
    private StateInfoMapper stateInfoMapper;

    @Override
    public List<CustomerVO> queryCustInfo() {
        List<Customer> customers = customerMapper.selectByExample(new CustomerExample());
        List<CustomerVO> customerVOS = CustomerVOConvert.INSTANCE.toVO(customers);
        Assert.notNull(customers, "列表为空");

        return customerVOS;
    }

    @Transactional
    @Override
    public MsgInter<?> updata(Customer customerDTO) {
        Assert.notNull(customerDTO.getCid(), "id为空");
        int update = customerMapper.updateByPrimaryKey(customerDTO);
        Follow follow = new Follow();
        follow.setCid(customerDTO.getCid());
        follow.setFState("未接触");
        followMapper.insert(follow);
        if (update <= 0) {
            return MsgAction.getError("修改失败!");
        }
        return MsgAction.getSuccess("修改成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> add(Customer customer) {
        int insert = customerMapper.insert(customer);
        if (insert <= 0) {
            return MsgAction.getSuccess("添加失败!");
        }

        //添加客户成功后，添加一条状态记录
        State state = new State();
        state.setName(customer.getcName());

        //先查询该用户是否已存在订单，如果是则获取订单数量，如果不是则订单数量为0；
        OrdersExample example = new OrdersExample();
        example.createCriteria().andCustEqualTo(customer.getcName());
        List<Orders> orders = ordersMapper.selectByExample(example);

        if (orders == null) {
            state.setState("脱离状态");
            state.setOrdnum(0);
        } else {
            state.setOrdnum(orders.size());
            List<StateInfo> stateInfos = stateInfoMapper.selectByExample(new StateInfoExample());
           for (int i=0 ;i<stateInfos.size();i++){
               if(orders.size() <= stateInfos.get(i).getSnum() && i==0){
                   state.setState(stateInfos.get(i).getState());
                   break;
               } else if (orders.size() <= stateInfos.get(i).getSnum() && orders.size() > stateInfos.get(i-1).getSnum()){
                   state.setState(stateInfos.get(i).getState());
                   break;
               } else {
                   state.setState("良好状态");
               }
           }
        }

        int insert1 = stateMapper.insert(state);
        if (insert1 <= 0) {
            return MsgAction.getError("客户状态添加失败！");
        }

        //增加一条最新消息
        Record record = new Record();
        record.setCreatetime(new Date());
        record.setContent("今日信息：与客户：" + customer.getcName() + "签约成功！祝贺祝贺~~~");
        int insert2 = recordMapper.insert(record);
        if (insert2 <= 0) {
            return MsgAction.getError("消息添加失败!");
        }

        return MsgAction.getSuccess("添加成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> deleteById(Integer id) {
        Assert.notNull(id, "id不存在");

        //先查询该id是否有对应的客户信息
        Customer customer = customerMapper.selectByPrimaryKey(id);
        Assert.notNull(customer, "客户信息不存在！");
        //查询该客户信息的状态
        StateExample example = new StateExample();
        example.createCriteria().andNameEqualTo(customer.getcName());
        List<State> states = stateMapper.selectByExample(example);

        Assert.notNull(states, "客户状态信息为空！");

        int delete = customerMapper.deleteByPrimaryKey(id);
        if (delete <= 0) {
            return MsgAction.getError("删除失败!");
        }

        int delete1 = stateMapper.deleteByPrimaryKey(states.get(0).getId());

        if (delete1 < 0) {
            return MsgAction.getError("客户状态清除失败!");
        }

        return MsgAction.getSuccess("删除成功!");
    }


    @Override
    public PageInfo<CustomerVO> getPageInfo(PageInfo<?> pageInfo, HttpServletRequest request) {
        Assert.notNull(pageInfo, "分页信息不存在");

        PageHelper.startPage(
                ObjectUtils.defaultIfNull(pageInfo.getPageNum(), 1),
                ObjectUtils.defaultIfNull(pageInfo.getPageSize(), 5));
        List<Customer> customers;
        if ("user".equals(request.getHeader("roles"))){
            customers = customerMapper.selectBynull();
        }else{
            customers = customerMapper.selectByExample(new CustomerExample());
        }

        PageInfo<Customer> voPageInfo = new PageInfo<>(customers);

        List<CustomerVO> customerVOS = CustomerVOConvert.INSTANCE.toVO(voPageInfo.getList());

        PageInfo<CustomerVO> info = new PageInfo<>();
        BeanUtils.copyProperties(voPageInfo, info);

        info.setList(customerVOS);

        return info;
    }

    @Override
    public MsgInter<?> getCustByName(String custName) {
        Assert.notNull(custName, "查询不存在");
        CustomerExample example = new CustomerExample();
        example.createCriteria().andCNameEqualTo(custName);
        List<Customer> customers = customerMapper.selectByExample(example);
        if (customers.isEmpty()) {
            return MsgAction.getError("此客户不存在");
        }
        return MsgAction.getSuccess();
    }

    @Transactional
    @Override
    public MsgInter<?> updateOfSea(Integer cid,Integer fid) {
        Customer customer = new Customer();
        customer.setCid(cid);
        Integer customerId = customerMapper.updateOfSea(customer);
        followMapper.deleteByPrimaryKey(fid);
        if (customerId!=1) {
            return MsgAction.getError("转入失败");
        }
        return MsgAction.getSuccess("修改成功");
    }


    public CustomerExample getExample() {
        return new CustomerExample();
    }

}
