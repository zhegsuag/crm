package ytu.zs.crm.service.impl;

import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.mapper.AccountMapper;
import ytu.zs.crm.pojo.Account;
import ytu.zs.crm.pojo.AccountExample;
import ytu.zs.crm.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @Description:
 * @author: zhegsuag
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private AccountMapper account;

    /**
     * 登陆验证
     *
     * @param username 用户名
     * @param password 密码
     * @return 返回ok和username
     */
    @Override
    public MsgAction<String> login(String username, String password) {

        Assert.notNull(username, "");
        Assert.notNull(password, "");
        Account account = new Account();
        account.setUsername(username);
        account.setPassword(password);
        List<Account> accounts = this.account.selectoflogin(account);
        if (accounts.isEmpty()) {
            return MsgAction.getError("用户名或者密码有误！");
        }
        return MsgAction.ok(username);

    }

    /**
     * 登陆验证
     *
     * @param username 用户名
     * @param password 密码
     * @return 返回ok和Account
     */
    @Override
    public MsgAction<Account> persion(String username, String password) {

        Assert.notNull(username, "");
        Assert.notNull(password, "");
        AccountExample example = new AccountExample();
        example.createCriteria().andUsernameEqualTo(username);

        List<Account> account = this.account.selectByExample(example);
        if (account.isEmpty()) {
            return MsgAction.getError("用户名不存在");
        }
        if (!account.get(0).getUsername().equals(username) || !account.get(0).getPassword().equals(password)) {
            return MsgAction.getError("用户名或者密码有误！");
        }
        return MsgAction.ok(account.get(0));

    }

    /**
     * 登陆验证
     *
     * @param account 用户
     * @return 返回ok和Account
     */
    @Override
    public MsgAction<Account> updatepersion(Account account) {
        Integer sum=this.account.updateByPrimaryKeySelective(account);
        if (sum==1) {
            return MsgAction.ok(this.account.selectByPrimaryKey(account.getaId()));
        }else{
            return MsgAction.error(account);
        }
    }


    /**
     * 信息重复验证
     *
     * @param account 用户
     * @return 返回ok和Account
     */
    @Override
    public MsgAction<Integer> selectinformation(Account account) {
        Integer sum = this.account.selectinformation(account);
        if(sum != null){
            return MsgAction.ok(sum);
        }
        return MsgAction.ok(0);
    }

    @Override
    public Account selectofname(String name) {
        return this.account.selectofname(name);
    }


}
