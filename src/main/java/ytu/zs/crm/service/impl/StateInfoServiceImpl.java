package ytu.zs.crm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.mapper.StateInfoMapper;
import ytu.zs.crm.mapper.StateMapper;
import ytu.zs.crm.pojo.State;
import ytu.zs.crm.pojo.StateExample;
import ytu.zs.crm.pojo.StateInfo;
import ytu.zs.crm.pojo.StateInfoExample;
import ytu.zs.crm.service.StateInfoService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName: StateInfoServiceImpl
 * @Author: zhegsuag
 * @Date: 2022/5/21 18:22
 * @Version: 1.0
 **/
@Service
public class StateInfoServiceImpl implements StateInfoService {

    @Autowired
    private StateInfoMapper stateInfoMapper;

    @Autowired
    private StateMapper stateMapper;

    @Override
    public MsgInter<?> getStateInfo() {
        return  MsgAction.ok(stateInfoMapper.selectByExample(new StateInfoExample()));
    }

    @Transactional
    @Override
    public MsgInter<?> get(List<StateInfo> list) {
        Collections.sort(list);
        stateInfoMapper.deleteByExample(new StateInfoExample());
        list.forEach(info->stateInfoMapper.insert(info));
        List<State> stateList = stateMapper.selectByExample(new StateExample());
        stateMapper.deleteByExample(new StateExample());
        stateList.forEach(state->{
            for(int i=0 ;i<list.size();i++){
                if(state.getOrdnum() <= list.get(i).getSnum() && i==0){
                    state.setState(list.get(i).getState());
                    break;
                } else if (state.getOrdnum() <= list.get(i).getSnum() && state.getOrdnum() > list.get(i-1).getSnum()){
                    state.setState(list.get(i).getState());
                    break;
                } else {
                    state.setState("良好状态");
                }
            }
            stateMapper.insert(state);
        });

        return MsgAction.ok(list);
    }
}
