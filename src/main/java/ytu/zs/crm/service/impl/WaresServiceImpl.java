package ytu.zs.crm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.mapper.WaresMapper;
import ytu.zs.crm.pojo.Wares;
import ytu.zs.crm.pojo.WaresExample;
import ytu.zs.crm.service.WaresService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @description: 商品类别
 * @author: zhegsuag
 */
@Service
public class WaresServiceImpl implements WaresService {

    @Autowired
    private WaresMapper waresMapper;

    @Override
    public MsgInter<?> getWaresInfo() {

        List<Wares> wares = waresMapper.selectByExample(new WaresExample());
        Assert.notNull(wares, "查询类别为空");
        return MsgAction.ok(wares);
    }

    @Override
    public MsgInter<?> getWaresByName(String name) {
        Assert.notNull(name, "");
        WaresExample example = new WaresExample();
        example.createCriteria().andWNameEqualTo(name);
        List<Wares> wares = waresMapper.selectByExample(example);
        return MsgAction.ok(wares.get(0));
    }

    @Override
    public PageInfo<?> getPageInfo(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息不存在");

        PageHelper.startPage(
                ObjectUtils.defaultIfNull(pageInfo.getPageNum(), 1),
                ObjectUtils.defaultIfNull(pageInfo.getPageSize(), 5));

        List<Wares> wares = waresMapper.selectByExample(new WaresExample());

        return new PageInfo<>(wares);
    }

    @Transactional
    @Override
    public MsgInter<?> delete(Integer id) {
        Assert.notNull(id, "");
        int delete = waresMapper.deleteByPrimaryKey(id);
        if (delete <= 0) {
            return MsgAction.getError("删除失败!");
        }
        return MsgAction.getSuccess("删除成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> addWares(Wares wares) {
        Assert.notNull(wares, "");
        int insert = waresMapper.insert(wares);
        if (insert <= 0) {
            return MsgAction.getError("添加失败!");
        }
        return MsgAction.getSuccess("添加成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> update(Wares wares) {
        Assert.notNull(wares.getWid(), "id不存在");
        int update = waresMapper.updateByPrimaryKey(wares);
        if (update <= 0) {
            return MsgAction.getError("修改失败!");
        }
        return MsgAction.getSuccess("修改成功!");
    }
}
