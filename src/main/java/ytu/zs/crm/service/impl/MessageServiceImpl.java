package ytu.zs.crm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.common.utils.APIUtil;
import ytu.zs.crm.common.utils.Epidemic;
import ytu.zs.crm.mapper.MessageMapper;
import ytu.zs.crm.pojo.Message;
import ytu.zs.crm.service.MessageService;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: MessageServiceImpl
 * @Description: TODO
 * @Author: zhegsuag
 * @Date: 2022/4/19 18:49
 * @Version: 1.0
 **/
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageMapper messageMapper;

    @Override
    public void sendMessages() {
        try {
            Message message = new Message();
            message.setmDate(new Date(System.currentTimeMillis()-1000 * 60 * 60 * 12));
            message.setMessage("【系统通知】该系统将于次日凌晨2点到5点进行升级维护");
            message.setmState(0);
            messageMapper.insert(message);
            message.setmDate(new Date());
            message.setMessage(Epidemic.getAreaStat());
            messageMapper.insert(message);
            message.setMessage(APIUtil.getWeatherByCity("烟台"));
            messageMapper.insert(message);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public List<Message> pading(Integer state) {
        return messageMapper.pading(state);
    }

    @Override
    public MsgInter<?> sync(Integer mid,Integer state) {
        Integer sum = null;
        if (mid ==0 ){
            sum = messageMapper.updateAll(mid,state);
        }else {
            sum = messageMapper.update(mid,state);
        }
        if (sum!=1){
            return MsgAction.error("修改失败");
        }else {
            return MsgAction.SUCCESS;
        }
    }
}
