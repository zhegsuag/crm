package ytu.zs.crm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.orders.vo.OrdersVO;
import ytu.zs.crm.controller.orders.vo.OrdersVOConvert;
import ytu.zs.crm.mapper.*;
import ytu.zs.crm.pojo.*;
import ytu.zs.crm.service.OrdersService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;

/**
 * @description: 订单详情
 * @author: zhegsuag
 */
@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private DetailMapper detailMapper;
    @Autowired
    private WaresMapper waresMapper;
    @Autowired
    private RecordMapper recordMapper;
    @Autowired
    private TextsMapper textsMapper;
    @Autowired
    private StateMapper  stateMapper;

    @Override
    public MsgInter<?> getOrdersInfo() {

        List<Orders> orders = ordersMapper.selectByExample(new OrdersExample());

        List<OrdersVO> ordersVOS = OrdersVOConvert.INSTANCE.toVO(orders);

        Assert.notNull(orders, "查询结果为空");
        return MsgAction.ok(ordersVOS);
    }

    @Override
    public PageInfo<?> getPaging(PageInfo<?> pageInfo) {
        PageHelper.startPage(
                ObjectUtils.defaultIfNull(pageInfo.getPageNum(), 1),
                ObjectUtils.defaultIfNull(pageInfo.getPageSize(), 5));

        List<Orders> orders = ordersMapper.selectByExample(new OrdersExample());
        Assert.notNull(orders, "查询列表为空");
        PageInfo<Orders> ordersPageInfo = new PageInfo<>(orders);

        List<Orders> ordersVOS =ordersPageInfo.getList();

        PageInfo<Orders> info = new PageInfo<>();

        BeanUtils.copyProperties(ordersPageInfo, info);

        info.setList(ordersVOS);


        return info;
    }

    @Transactional
    @Override
    public MsgInter<?> addOrders(Orders orders) {
        Assert.notNull(orders, "订单为空");
        List<Orders> ordersList = ordersMapper.selectByExample(new OrdersExample());
        orders.setOid(ordersList.get(ordersList.size()-1).getOid()+1);
        WaresExample example = new WaresExample();
        example.createCriteria().andWNameEqualTo(orders.getwName1());
        List<Wares> wares = waresMapper.selectByExample(example);

        double total = wares.get(0).getwPrice() * orders.getNumber1();
        orders.setTotal(total);
        int insert = ordersMapper.insert(orders);
        if (insert <= 0) {
            return MsgAction.getError("添加失败!");
        }

        //订单添加成功后，增加一条订单明细
        Detail detail = new Detail();
        detail.setOid(orders.getOid());
        detail.setWid(wares.get(0).getWid());
        detail.setCreatetime(new Date());
        detail.setState((byte) 0);

        int insert1 = detailMapper.insert(detail);
        if (insert1 <= 0) {
            return MsgAction.getError("订单明细添加失败!");
        }

        //增加订单明细成功后，增加一条最新消息
        Record record = new Record();
        record.setCreatetime(new Date());
        record.setContent("今日信息：客户" + "<" + orders.getCust() + ">" + "订购了" + orders.getwName1() + "等~~~");
        int insert2 = recordMapper.insert(record);
        if (insert2 <= 0) {
            return MsgAction.getError("消息添加失败!");
        }

        //增加一条待办事项
        Texts texts = new Texts();
        texts.setOid(orders.getOid());
        texts.setText("有一个编号为：" + orders.getOid() + "的订单未处理噢!!!赶紧去看看吧~");
        texts.setFinish((byte) 0);
        int insert3 = textsMapper.insert(texts);
        if (insert3 <= 0) {
            return MsgAction.getError("待办事项添加失败!");
        }

        //更新该客户的状态信息
        updateState(orders.getCust(),"insert");

        return MsgAction.getSuccess("添加成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> deleteById(Integer id) {

        Orders orders = ordersMapper.selectByPrimaryKey(id);
        String cust = orders.getCust();

        Assert.notNull(id, "id为空");
        int delete = ordersMapper.deleteByPrimaryKey(id);
        DetailExample example = new DetailExample();
        example.createCriteria().andOidEqualTo(id);
        detailMapper.deleteByExample(example);
        if (delete <= 0) {
            return MsgAction.getError("删除失败!");
        }

        updateState(cust,"delete");

        return MsgAction.getSuccess("删除成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> update(Orders orders) {
        Assert.notNull(orders.getOid(), "id不存在");
        int update = ordersMapper.updateByPrimaryKey(orders);
        if (update <= 0) {
            return MsgAction.getError("修改失败!");
        }
        return MsgAction.getSuccess("修改成功!");
    }

    public void updateState(String name, String method) {
        //更新该客户的状态信息
        StateExample stateExample = new StateExample();
        stateExample.createCriteria().andNameEqualTo(name);
        List<State> states = stateMapper.selectByExample(stateExample);
        State state = states.get(0);

        Integer ordNum = state.getOrdnum();
        if (ordNum >= 5) {
            state.setState("良好");
        }
        if (ordNum > 0 && ordNum < 5) {
            state.setState("危险状态");
        }
        if (ordNum == 0) {
            state.setState("脱离状态");
        }

            switch (method) {
                case "insert":
                    ordNum += 1;
                    break;
                case "delete":
                    ordNum -= 1;
            }
        if (ordNum < 0) {
            return;
        }
        state.setOrdnum(ordNum);

        stateMapper.updateByExample(state, stateExample);
    }
}
