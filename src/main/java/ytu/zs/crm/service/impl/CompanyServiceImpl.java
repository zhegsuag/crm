package ytu.zs.crm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.company.vo.CompanyVO;
import ytu.zs.crm.controller.company.vo.CompanyVOConvert;
import ytu.zs.crm.mapper.CompanyMapper;
import ytu.zs.crm.pojo.Company;
import ytu.zs.crm.pojo.CompanyExample;
import ytu.zs.crm.service.CompanyService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @description: 公司信息
 * @author: zhegsuag
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyMapper companyMapper;

    @Override
    public List<Company> getAllList() {
        List<Company> companyList = companyMapper.selectByExample(new CompanyExample());
        Assert.notNull(companyList, "列表为空");
        return companyList;
    }

    @Override
    public PageInfo<?> getPageInfo(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息为空");

        PageHelper.startPage(
                ObjectUtils.defaultIfNull(pageInfo.getPageNum(), 0),
                ObjectUtils.defaultIfNull(pageInfo.getPageSize(), 5));

        List<Company> companies = companyMapper.selectByExample(new CompanyExample());

        PageInfo<Company> companyPageInfo = new PageInfo<>(companies);

        List<CompanyVO> companyVOS = CompanyVOConvert.INSTANCE.toVO(companyPageInfo.getList());

        PageInfo<CompanyVO> info = new PageInfo<>();

        BeanUtils.copyProperties(companyPageInfo, info);
        info.setList(companyVOS);

        return info;
    }

    @Transactional
    @Override
    public MsgInter<?> addCompany(Company company) {
        Assert.notNull(company, "");
        int insert = companyMapper.insert(company);
        if (insert <= 0) {
            return MsgAction.getError("添加失败!");
        }
        return MsgAction.getSuccess("添加成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> update(Company company) {
        Assert.notNull(company.getCpid(), "id不存在");
        int update = companyMapper.updateByPrimaryKey(company);
        if (update <= 0) {
            return MsgAction.getError("修改失败!");
        }
        return MsgAction.getSuccess("修改成功!");
    }

    @Override
    public MsgInter<?> delete(Integer id) {
        Assert.notNull(id, "id不存在");
        int delete = companyMapper.deleteByPrimaryKey(id);
        if (delete <= 0) {
            return MsgAction.getError("删除失败!");
        }
        return MsgAction.getSuccess("删除成功!");
    }
}
