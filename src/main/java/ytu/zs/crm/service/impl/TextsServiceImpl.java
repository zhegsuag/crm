package ytu.zs.crm.service.impl;

import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.texts.dto.TextsDTO;
import ytu.zs.crm.controller.texts.dto.TextsDTOConvert;
import ytu.zs.crm.controller.texts.vo.TextsVO;
import ytu.zs.crm.controller.texts.vo.TextsVOConvert;
import ytu.zs.crm.mapper.TextsMapper;
import ytu.zs.crm.pojo.Texts;
import ytu.zs.crm.service.TextsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 待办事项
 * @author: zhegsuag
 */
@Service
public class TextsServiceImpl implements TextsService {

    @Autowired
    private TextsMapper textsMapper;

    @Override
    public MsgInter<?> getInfo() {
        List<TextsDTO> info = textsMapper.getInfo();

        List<TextsVO> textsVOS = new ArrayList<>();

        for (int i = 0; i < info.size(); i++) {
            TextsVO textsVO = TextsVOConvert.INSTANCE.toVO(info.get(i));
            if (info.get(i).getFinish() == 0) {
                textsVO.setFinish(false);
                textsVOS.add(textsVO);
            }
        }
        return MsgAction.ok(textsVOS);
    }

    @Transactional
    @Override
    public MsgInter<?> addText(Texts texts) {
        Assert.notNull(texts.getText(), "待办不存在！");
        texts.setFinish((byte) 0);
        int insert = textsMapper.insert(texts);
        if (insert <= 0) {
            return MsgAction.getError("添加失败！");
        }
        return MsgAction.getSuccess("添加成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> update(TextsVO textsVO) {
        Assert.notNull(textsVO.getTid(), "事项为空");
        Texts texts = TextsDTOConvert.INSTANCE.toDTO(textsVO);
        if (textsVO.isFinish()) {
            texts.setFinish((byte) 1);
        } else {
            texts.setFinish((byte) 0);
        }
        int update = textsMapper.updateByPrimaryKey(texts);
        if (update <= 0) {
            return MsgAction.getError("修改错误!");
        }
        return MsgAction.getSuccess("事项完成!");
    }
}
