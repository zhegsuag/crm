package ytu.zs.crm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.log.pojo.SysOperLog;
import ytu.zs.crm.mapper.AccountMapper;
import ytu.zs.crm.pojo.*;
import ytu.zs.crm.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: 账户信息
 * @author: zhegsuag
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Transactional
    @Override
    public List<Account> getAllList() {
        List<Account> list = accountMapper.selectByExample(new AccountExample());
        Assert.notNull(list, "列表为空");
        return list;
    }

    @Transactional
    @Override
    public PageInfo<?> paging(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息为空");
        PageHelper.startPage(
                ObjectUtils.defaultIfNull(pageInfo.getPageNum(), 0),
                ObjectUtils.defaultIfNull(pageInfo.getPageSize(), 10));
        List<Account> accounts = accountMapper.selectByExample(new AccountExample()).stream().filter(a->a.getaId()!=0).collect(Collectors.toList());
        PageInfo<Account> logPageInfo = new PageInfo<>(accounts);
        return logPageInfo;
    }

    @Transactional
    @Override
    public MsgInter<?> deleteById(Integer id) {
        Assert.notNull(id, "id不存在");
        //先查询该id是否有对应的客户信息
        Account account = accountMapper.selectByPrimaryKey(id);
        Assert.notNull(account, "用户信息不存在！");

        int delete = accountMapper.deleteByPrimaryKey(id);
        if (delete <= 0) {
            return MsgAction.getError("删除失败!");
        }
        return MsgAction.getSuccess("删除成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> update(Account account) {
        Assert.notNull(account.getaId(), "id为空");
        int update = accountMapper.updateByPrimaryKey(account);
        if (update <= 0) {
            return MsgAction.getError("修改失败!");
        }
        return MsgAction.getSuccess("修改成功!");
    }

    @Transactional
    @Override
    public MsgInter<?> addAccount(Account account) {
        account.setHeadImg("https://cube.elemecdn.com/9/c2/f0ee8a3c7c9638a54940382568c9dpng.png");
        account.setCreatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        int insert = accountMapper.insert(account);
        if (insert <= 0) {
            return MsgAction.getSuccess("添加失败!");
        }
        return MsgAction.getSuccess("添加成功!");
    }
}
