package ytu.zs.crm.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.stereotype.Service;
import ytu.zs.crm.pojo.Account;
import ytu.zs.crm.service.TokenService;

import java.util.Date;

/**
 * @ClassName: TokenServiceImpl
 * @Author: zhegsuag
 * @Version: 1.0
 **/
@Service
public class TokenServiceImpl implements TokenService {
    @Override
    public String getToken(Account account) {
        Date start = new Date();
        long currentTime = System.currentTimeMillis() + 60 * 60 * 1000;//一小时有效时间
        Date end = new Date(currentTime);
        String token = "";
        token = JWT.create().withAudience(account.getUsername()).withSubject("scc").withIssuedAt(start).withExpiresAt(end)
                .sign(Algorithm.HMAC256(account.getPassword()));
        return token;
    }
}
