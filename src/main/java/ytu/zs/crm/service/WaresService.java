package ytu.zs.crm.service;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.pojo.Wares;

/**
 * @description: 商品类别
 * @author: zhegsuag
 */
public interface WaresService {

    /**
     * @return 返回商品列表
     */
    MsgInter<?> getWaresInfo();

    /**
     *
     * @param name 商品名称
     * @return 返回商品信息
     */
    MsgInter<?> getWaresByName(String name);

    /**
     *
     * @param pageInfo 分页信息
     * @return 返回分页列表
     */
    PageInfo<?> getPageInfo(PageInfo<?> pageInfo);

    /**
     *
     * @param id 商品id
     * @return 返回删除行数
     */
    MsgInter<?> delete(Integer id);

    /**
     *
     * @param wares 商品信息
     * @return 返回添加成功
     */
    MsgInter<?> addWares(Wares wares);

    /**
     *
     * @param wares 商品信息
     * @return 返回修改成功
     */
    MsgInter<?> update(Wares wares);
}
