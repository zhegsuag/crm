package ytu.zs.crm.service;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.cust.vo.CustomerVO;
import ytu.zs.crm.pojo.Customer;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author: zhegsuag
 */
public interface CustInfoService {

    /**
     *
     * @return 返回客户列表
     */
    List<CustomerVO> queryCustInfo();

    /**
     *
     * @param customer 客户信息
     * @return 返回修改成功
     */
    MsgInter<?> updata(Customer customer);

    /**
     *
     * @param customer 客户信息
     * @return 返回添加成功
     */
    MsgInter<?> add(Customer customer);

    /**
     *
     * @param id 客户id
     * @return 返回删除成功
     */
    MsgInter<?> deleteById(Integer id);

    /**
     *
     * @param pageInfo 分页信息
     * @param request
     * @return 返回分页列表
     */
    PageInfo<CustomerVO> getPageInfo(PageInfo<?> pageInfo, HttpServletRequest request);

    /**
     *
     * @param custName 客户名称
     * @return 返回该客户名称信息
     */
    MsgInter<?> getCustByName(String custName);

    MsgInter<?> updateOfSea(Integer cid,Integer fid);

}
