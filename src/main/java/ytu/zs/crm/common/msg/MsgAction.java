package ytu.zs.crm.common.msg;


import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.util.Objects;

/**
 * 消息对象 <br>
 * <li>成功代码：ok</li>
 * <li>默认错误代码：error</li>
 *
 * @Author zhegsuag
 */
public class MsgAction<T> implements MsgInter<T> {

    private final String code;           //消息代码
    private       String msg;
    private       T      data;            //消息内容

    /**
     * 成功代码
     */
    public static final String SUCCESS_CODE = "ok";
    /**
     * 默认错误代码
     */
    public static final String ERROR_CODE   = "error";

    /**
     * 成功消息
     * <li>成功代码：ok</li>
     *
     */
    public static final MsgAction SUCCESS = new MsgAction(SUCCESS_CODE, "操作成功") {
        private static final long serialVersionUID = 6912337066621277932L;

        @Override
        public MsgAction setData(Object data) {
            throw new UnsupportedOperationException("不允许该操作");
        }

        @Override
        public Object getData() {
            return null;
        }

        @Override
        public MsgAction setMsg(String msg) {
            throw new UnsupportedOperationException("不允许该操作");
        }
    };
    /**
     * 错误消息
     * <li>错误代码：error</li>
     *
     * @Author zhegsuag
     */
    public static final MsgAction ERROR   = new MsgAction(ERROR_CODE, "操作失败") {
        private static final long serialVersionUID = -1529524511756870544L;

        @Override
        public MsgAction setData(Object data) {
            throw new UnsupportedOperationException("不允许该操作");
        }

        @Override
        public Object getData() {
            return null;
        }

        @Override
        public MsgAction setMsg(String msg) {
            throw new UnsupportedOperationException("不允许该操作");
        }
    };

    public MsgAction(String code, String msg) {
        this.code = code;
        Assert.hasText(this.code, "not null");
        this.msg = StringUtils.abbreviate(msg, 250);
    }


    /**
     * <li>成功代码：ok</li>
     *
     */
    public MsgAction(MsgInter<?> msgInter) {
        Assert.notNull(msgInter, "not null");
        Assert.hasText(msgInter.getCode(), "not null");
        this.msg = msgInter.getMsg();
        this.code = msgInter.getCode();
        if (msgInter.hasError() != this.hasError()) {
            throw new IllegalStateException("不能从一个删除消息转成正确消息");
        }
    }

    /**
     * 创建【成功消息】
     * <li>成功代码：ok</li>
     */
    public static <R> MsgAction<R> getSuccess() {
        return new MsgAction<>(SUCCESS_CODE, null);
    }

    /**
     * 创建【成功消息】
     * <li>成功代码：ok</li>
     */
    public static <R> MsgAction<R> ok(R data) {
        MsgAction<R> msgAction = new MsgAction<>(SUCCESS_CODE, null);
        return msgAction.setData(data);
    }

    /**
     * 创建【成功消息】
     * <li>成功代码：ok</li>
     */
    public static <R> MsgAction<R> getSuccess(String msg) {
        return new MsgAction<>(SUCCESS_CODE, msg);
    }


    /**
     * 创建【错误消息】 <br>
     * <li>错误代码：error</li>
     */
    public static <R> MsgAction<R> getError() {
        return new MsgAction<>(ERROR_CODE, null);
    }

    /**
     * 创建【错误消息】 <br>
     * <li>错误代码：error</li>
     */
    public static <R> MsgAction<R> getError(String msg) {
        return new MsgAction<>(ERROR_CODE, msg);
    }


    /**
     * 创建【错误消息】
     * <li>错误代码：error</li>
     */
    public static <R> MsgAction<R> error(R data) {
        MsgAction<R> msgAction = new MsgAction<>(ERROR_CODE, null);
        return msgAction.setData(data);
    }

    /**
     * 是否错误 <br>
     * <li>成功代码：ok</li>
     *
     * @return 返回
     */
    @Override
    public boolean hasError() {
        return !Objects.equals(SUCCESS_CODE, this.getCode());
    }


    @Override
    public MsgAction<T> setData(T data) {
        this.data = data;
        return this;
    }

    @Override
    public MsgAction<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    /**
     * 获取消息代码
     *
     * @return
     */
    @Override
    public String getCode() {
        return this.code;
    }

    /**
     * 获取消息
     *
     * @return
     */
    @Override
    public String getMsg() {
        return this.msg;
    }

    /**
     * 获取数
     *
     * @author zhegsuag
     */
    @Override
    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return String.format("{\"code\":\"%s\",\"msg\":\"%s\"}", getCode(), getMsg());
    }

}
