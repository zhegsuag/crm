package ytu.zs.crm.common.msg;


import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

/**
 * Msg 消息异常
 *
 * @author zhegsuag
 */
public class MsgException extends RuntimeException {
    private static final long serialVersionUID = -90782671136323712L;

    private final MsgInter<?> msgInter;

    public MsgException(String message, MsgInter<?> msgInter) {
        super(StringUtils.defaultIfBlank(message, msgInter.getMsg()));
        Assert.notNull(msgInter,"not null");
        this.msgInter = msgInter;
    }

    public MsgException(MsgInter<?> msgInter) {
        super(msgInter.getMsg());
        Assert.notNull(msgInter,"not null");
        this.msgInter = msgInter;
    }

    public MsgInter<?> getMsgInter() {
        return msgInter;
    }

}
