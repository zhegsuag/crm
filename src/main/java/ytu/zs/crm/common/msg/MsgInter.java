package ytu.zs.crm.common.msg;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 统一消息对象接口
 *
 * @author zhegsuag
 */
public interface MsgInter<T> extends Serializable {

    /**
     * 获取代码
     */
    String getCode();

    /**
     * 获取消息
     */
    String getMsg();

    /**
     * 获取数据
     */
    T getData();

    /**
     * 设置数据,返回自身
     */
    MsgInter<T> setData(T data);

    /**
     * 设置消息,返回自身
     */
    MsgInter<T> setMsg(String msg);

    /**
     * 是否是错误的消息
     */
    boolean hasError();

    /**
     * 返回消息数据，如果错误则抛出异常
     *
     * @return
     * @throws MsgException
     */
    default Optional<T> fetch() throws MsgException {
        this.throwIfError();
        return Optional.ofNullable(getData());
    }

    /**
     * 如果存在错误，就执行函数
     *
     * @param function
     * @param <R>
     * @return
     */
    default <R> Optional<R> ifError(Function<MsgInter<T>, Optional<R>> function) {
        if (this.hasError()) {
            return function.apply(this);
        }
        return Optional.empty();
    }


    /**
     * 如果成功，就执行函数
     *
     * @param function
     * @param <R>
     * @return
     */
    default <R> Optional<R> ifSuccess(Function<MsgInter<T>, Optional<R>> function) {
        if (!this.hasError()) {
            return function.apply(this);
        }
        return Optional.empty();
    }

    /**
     * 如果失败返回自身，如果成功那么就执行
     *
     * @author zhegsuag
     */
    default MsgInter<T> map(Supplier<MsgInter<T>> mapper) {
        Assert.notNull(mapper, "not null");

        if (hasError()) {
            return this;
        } else {
            return mapper.get();
        }
    }

    /**
     * 如果消息是错误则抛出异常
     *
     * @throws MsgException
     */
    default void throwIfError() throws MsgException {
        throwIfError(this.getMsg());
    }

    /**
     * 如果消息是错误则抛出异常
     *
     * @param msg
     * @param args
     * @throws MsgException
     * @example {@code #throwIfError("服务异常:%s","30")}
     */
    default void throwIfError(String msg, Object... args) throws MsgException {
        if (!this.hasError()) {
            return;
        }

        if (msg != null && ArrayUtils.isNotEmpty(args)) {
            msg = String.format(msg, args);
        } else {
            msg = ObjectUtils.firstNonNull(msg, getMsg(), "");
        }

        throw new MsgException(msg, this);
    }

    /**
     * 自定义异常抛出
     *
     * @param exceptionSupplier
     * @param <X>
     * @throws X
     * @example {@code #throwIfError(Exception:new)}
     */
    default <X extends Throwable> void throwIfError(Function<MsgInter<T>, ? extends X> exceptionSupplier) throws X {
        if (this.hasError()) {
            throw exceptionSupplier.apply(this);
        }
    }

}
