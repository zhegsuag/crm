package ytu.zs.crm.common.verify;

import ytu.zs.crm.common.verify.exception.VerifyException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;

import javax.validation.groups.Default;
import java.util.Optional;

/**
 * 验证接口
 *
 * @Author zhegsuag
 */
public interface Verify {

    /**
     * 如果没有指定验证的字段，那么验证所有字段
     *
     * @param fields 要验证的字段
     * @return
     */
    default Optional<String> verify(Class<? extends Default> groupClazz, String... fields) {
        String errorMsg = ValidatorUtils.validate(this, groupClazz, fields);
        if (StringUtils.isNotBlank(errorMsg)) {
            return Optional.of(errorMsg);
        }

        //额外验证
        errorMsg = this.extraVerify(groupClazz, fields);
        if (StringUtils.isNotBlank(errorMsg)) {
            return Optional.of(errorMsg);
        }

        return Optional.empty();
    }

    default Optional<String> verify(String... fields) {
        return this.verify(Default.class, fields);
    }

    /**
     * 如果没有指定验证的字段，那么验证所有字段，如果验证失败则抛出【VerifyException】
     *
     * @param fields 要验证的字段
     * @throws VerifyException
     */
    default void verifyThrow(String... fields) throws VerifyException {
        Optional<String> verifyOptional = this.verify(fields);
        if (verifyOptional.isPresent()) {
            throw new VerifyException(verifyOptional.get());
        }
    }


    default void verifyThrow(Class<? extends Default> groupClazz, String... fields) throws VerifyException {
        Optional<String> verifyOptional = this.verify(groupClazz, fields);
        if (verifyOptional.isPresent()) {
            throw new VerifyException(verifyOptional.get());
        }
    }

    /*
     * 如果存在错误，就抛出自定义异常
     *
     * @param exceptionSupplier
     * @param <X>
     * @throws X
     */
    //default <X extends Throwable> void verifyThrow(Supplier<? extends X> exceptionSupplier) throws X {
    //    Optional<String> verifyOptional = this.verify();
    //    if (verifyOptional.isPresent()) {
    //        X throwable = exceptionSupplier.get();
    //        ReflectUtils.setFieldValue(throwable, "detailMessage", verifyOptional.get());
    //        throw throwable;
    //    }
    //}

    @Nullable
    default String extraVerify(Class<? extends Default> groupClazz, String... fields) {
        return null;
    }

    /**
     * 更新
     *
     * @author zhegsuag
     */
    interface Update extends Default {
    }

    /**
     * 创建
     *
     * @author zhegsuag
     */
    interface Create extends Default {
    }

    /**
     * 其他
     *
     * @author zhegsuag
     */
    interface Other extends Default {
    }
}
