package ytu.zs.crm.common.verify.exception;

/**
 * 验证异常
 *
 * @author zhegsuag
 */
public class VerifyException extends RuntimeException {
    private static final long serialVersionUID = -8683840425590321681L;


    public VerifyException() {
        super();
    }


    public VerifyException(String message) {
        super(message);
    }


    public VerifyException(String message, Throwable cause) {
        super(message, cause);
    }


    public VerifyException(Throwable cause) {
        super(cause);
    }

    protected VerifyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
