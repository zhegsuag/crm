package ytu.zs.crm.common.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;


import ch.qos.logback.core.db.dialect.DBUtil;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @Author zhegsuag
 * @Description //获取烟台疫情
 **/

public class Epidemic {
    public static void main(String[] args) throws IOException, SQLException {
        getAreaStat();
    }


    private static String httpRequset(String requesturl) throws IOException {
        StringBuffer buffer = null;
        BufferedReader bufferedReader = null;
        InputStreamReader inputStreamReader = null;
        InputStream inputStream = null;
        HttpsURLConnection httpsURLConnection = null;
        try {
            URL url = new URL(requesturl);
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setRequestMethod("GET");
            inputStream = httpsURLConnection.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            bufferedReader = new BufferedReader(inputStreamReader);
            buffer = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return buffer.toString();
    }


    public static String getAreaStat() throws SQLException {
        String url = "https://ncov.dxy.cn/ncovh5/view/pneumonia";
        String htmlResult = "";
        try {
            htmlResult = httpRequset(url);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        String reg = "window.getAreaStat = (.*?)\\}(?=catch)";
        Pattern totalPattern = Pattern.compile(reg);
        Matcher totalMatcher = totalPattern.matcher(htmlResult);
        String result = "";
        StringBuilder str = null;
        if (totalMatcher.find()) {
            result = totalMatcher.group(1);
            /**
             * 截取烟台的疫情
             * currentConfirmedCount:现存确诊
             * confirmedCount:累计确诊
             * suspectedCount:疑似病例
             * deadCount:死亡
             * curedCount:治愈
             * highDangerCount:高风险区
             * midDangerCount:中风险区
             **/
            result = result.substring(result.indexOf("烟台") - 13);
            result = result.substring(0, result.indexOf('}') + 1);
            String currentConfirmedCount = result.substring(result.indexOf("currentConfirmedCount") + 23, result.indexOf("confirmedCount") - 2);
            String confirmedCount = result.substring(result.indexOf("confirmedCount") + 16, result.indexOf("suspectedCount") - 2);
            ;
            String suspectedCount = result.substring(result.indexOf("suspectedCount") + 16, result.indexOf("curedCount") - 2);
            String deadCount = result.substring(result.indexOf("deadCount") + 11, result.indexOf("highDangerCount") - 2);
            String curedCount = result.substring(result.indexOf("curedCount") + 12, result.indexOf("deadCount") - 2);
            String highDangerCount = result.substring(result.indexOf("highDangerCount") + 17, result.indexOf("midDangerCount") - 2);
            String midDangerCount = result.substring(result.indexOf("midDangerCount") + 16, result.indexOf("locationId") - 2);
            str = new StringBuilder();
            str = str.append("【疫情防控】")
                    .append("烟台市当前疫情情况如下：")
                    .append("现存确诊:").append(currentConfirmedCount).append(";")
                    .append("累计确诊:").append(confirmedCount).append(";")
                    .append("疑似病例:").append(suspectedCount).append(";")
                    .append("死亡:").append(deadCount).append(";")
                    .append("治愈:").append(curedCount).append(";")
                    .append("高风险区:").append(highDangerCount).append(";")
                    .append("中风险区:").append(midDangerCount).append(";")
                    .append("请注意工作中的疫情防控");

        }
        return String.valueOf(str);
    }
}
