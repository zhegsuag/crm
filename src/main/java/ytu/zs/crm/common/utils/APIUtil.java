package ytu.zs.crm.common.utils;

import net.sf.json.JSONObject;
import ytu.zs.crm.pojo.StateInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: APIUtil
 * @Description:
 * @Author: zhegsuagTODO https://blog.csdn.net/qq_41334351/article/details/103422744 调用天气
 * @Version: 1.0
 *
 **/
public class APIUtil {
    public static void main(String[] args) {
       /* String ipAddress = "182.34.34.137";
        String city = getCityByIP(ipAddress);*/
        System.out.println(getWeatherByCity("烟台"));;
    }



    //根据IP获取所在城市的名称
    public static String getCityByIP(String IP) {
        String city ="";
        String jsonStr = "";
        String strUrl = "http://apis.juhe.cn/ip/ipNew";
        String srtVlaue = "ip="+IP+"&key=022287affefd4e498740d2d107efbe4c";
        jsonStr = HttpGetPost.sendGet(strUrl,srtVlaue);
        System.out.println(jsonStr);
        if(jsonStr.equals("请求超时")) {
            return "获取城市名称失败";
        }
        JSONObject json = JSONObject.fromObject(jsonStr);
        String resultcode = (String) json.get("resultcode");
        if(resultcode.equals("200")) {//接口請求成功
            JSONObject result = (JSONObject) json.get("result");
            city = (String) result.get("City");
        }else if("127.0.0.1".equals(IP)){
            city = "内网IP";
        }
        else {
            city = "所在城市获取失败";
        }
        if(city.contains("市")) {
            city = city.substring(0, 2);//只截取前两位汉字
        }
        System.out.println("根据IP:"+IP+"获取城市名称为:"+city);
        return city;
    }

    //根据城市名称获取该城市的天气状况
    public static String getWeatherByCity(String City) {
        Map<String,Object> hashmap = new HashMap<String, Object>();
        String jsonStr = "";
        String strUrl = "http://apis.juhe.cn/simpleWeather/query";
        String srtVlaue = "city="+City+"&key=93e7573d94a0b85b2ac38ded5aef41e0";
        jsonStr = HttpGetPost.sendGet(strUrl,srtVlaue);
        JSONObject json = JSONObject.fromObject(jsonStr);
        String reason = (String) json.get("reason");
        StringBuilder str = new StringBuilder();
        if(reason.equals("查询成功!")) {
            jsonStr = jsonStr.substring(jsonStr.indexOf("future")+8);
            jsonStr = jsonStr.substring(jsonStr.indexOf("temperature"));
            String temperature = jsonStr.substring(jsonStr.indexOf("temperature")+13,jsonStr.indexOf("weather")-2);
            String weather = jsonStr.substring(jsonStr.indexOf("weather")+9,jsonStr.indexOf("wid")-3);
            str = str.append("【天气预报】").append("今日气温:").append(temperature).append(";")
                     .append("天气:").append(weather).append(";");
            jsonStr = jsonStr.substring(jsonStr.indexOf("direct"));
            str = str.append(jsonStr, jsonStr.indexOf("direct")+9, jsonStr.indexOf("}")).append(";  ");
            jsonStr = jsonStr.substring(jsonStr.indexOf('}')+2);
            temperature = jsonStr.substring(jsonStr.indexOf("temperature")+13,jsonStr.indexOf("weather")-2);
            weather = jsonStr.substring(jsonStr.indexOf("weather")+9,jsonStr.indexOf("wid")-3);
            str = str.append("明日气温:").append(temperature).append(";")
                    .append("天气:").append(weather).append(";");
            jsonStr = jsonStr.substring(jsonStr.indexOf("direct"));
            str = str.append(jsonStr, jsonStr.indexOf("direct")+9, jsonStr.indexOf("}"));
            return String.valueOf(str);

        }else {
            return "获取"+City+"天气失败";
        }
    }
}
