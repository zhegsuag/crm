package ytu.zs.crm.common.utils;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Collection;

/**
 * 分页结果
 *
 * @author zhegsuag
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class PageResult {

    private Long          total;
    private Collection<?> list;


    public PageResult(Collection<?> list, Long total) {
        this.list = list;
        this.total = total;
    }

    public static PageResult newInstance(Collection<?> list, Long total) {
        return new PageResult(list, total);
    }

}
