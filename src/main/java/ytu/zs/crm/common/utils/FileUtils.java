package ytu.zs.crm.common.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author zhegsuag
 */
public class FileUtils {

    /**
     * 生成一个新的文件名
     *
     * @param file
     * @return
     */
    public static String renameFile(MultipartFile file) {
        //原文件名
        String name = file.getOriginalFilename();
        //后缀
        String suffixName = name.substring(name.lastIndexOf("."));
        //新文件名
        name = UUID.randomUUID() + suffixName;

        return name;
    }

    /**
     * 创建文件
     *
     * @param file
     * @param savePath
     * @return
     */
    public static boolean transferToFile(MultipartFile file, String savePath) {
        File saveFile = new File(savePath);
        if (!saveFile.exists()) {
            saveFile.mkdirs();
        }
        try {
            //将临时存储的文件移动到真实存储路径下
            file.transferTo(saveFile);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
