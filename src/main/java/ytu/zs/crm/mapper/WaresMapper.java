package ytu.zs.crm.mapper;

import ytu.zs.crm.pojo.Wares;
import ytu.zs.crm.pojo.WaresExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WaresMapper {
    long countByExample(WaresExample example);

    int deleteByExample(WaresExample example);

    int deleteByPrimaryKey(Integer wid);

    int insert(Wares record);

    int insertSelective(Wares record);

    List<Wares> selectByExample(WaresExample example);

    Wares selectByPrimaryKey(Integer wid);

    int updateByExampleSelective(@Param("record") Wares record, @Param("example") WaresExample example);

    int updateByExample(@Param("record") Wares record, @Param("example") WaresExample example);

    int updateByPrimaryKeySelective(Wares record);

    int updateByPrimaryKey(Wares record);
}
