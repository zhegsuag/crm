package ytu.zs.crm.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ytu.zs.crm.pojo.StateInfo;
import ytu.zs.crm.pojo.StateInfoExample;
@Repository
public interface StateInfoMapper {
    long countByExample(StateInfoExample example);

    int deleteByExample(StateInfoExample example);

    int deleteByPrimaryKey(Integer sid);

    int insert(StateInfo row);

    int insertSelective(StateInfo row);

    List<StateInfo> selectByExample(StateInfoExample example);

    StateInfo selectByPrimaryKey(Integer sid);

    int updateByExampleSelective(@Param("row") StateInfo row, @Param("example") StateInfoExample example);

    int updateByExample(@Param("row") StateInfo row, @Param("example") StateInfoExample example);

    int updateByPrimaryKeySelective(StateInfo row);

    int updateByPrimaryKey(StateInfo row);
}
