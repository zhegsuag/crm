package ytu.zs.crm.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ytu.zs.crm.pojo.Message;
import ytu.zs.crm.pojo.MessageExample;
@Repository
public interface MessageMapper {
    long countByExample(MessageExample example);

    int deleteByExample(MessageExample example);

    int deleteByPrimaryKey(Integer mid);

    int insert(Message row);

    int insertSelective(Message row);

    List<Message> selectByExampleWithBLOBs(MessageExample example);

    List<Message> selectByExample(MessageExample example);

    Message selectByPrimaryKey(Integer mid);

    int updateByExampleSelective(@Param("row") Message row, @Param("example") MessageExample example);

    int updateByExampleWithBLOBs(@Param("row") Message row, @Param("example") MessageExample example);

    int updateByExample(@Param("row") Message row, @Param("example") MessageExample example);

    int updateByPrimaryKeySelective(Message row);

    int updateByPrimaryKeyWithBLOBs(Message row);

    int updateByPrimaryKey(Message row);

    List<Message> pading(Integer state);

    Integer update(Integer mid, Integer state);

    Integer updateAll(Integer mid, Integer state);
}
