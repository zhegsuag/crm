package ytu.zs.crm.mapper;

import ytu.zs.crm.controller.orders.dto.DetailDTO;
import ytu.zs.crm.pojo.Detail;
import ytu.zs.crm.pojo.DetailExample;
import ytu.zs.crm.pojo.DetailKey;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailMapper {
    long countByExample(DetailExample example);

    int deleteByExample(DetailExample example);

    int deleteByPrimaryKey(DetailKey key);

    int insert(Detail record);

    int insertSelective(Detail record);

    List<Detail> selectByExample(DetailExample example);

    Detail selectByPrimaryKey(DetailKey key);

    int updateByExampleSelective(@Param("record") Detail record, @Param("example") DetailExample example);

    int updateByExample(@Param("record") Detail record, @Param("example") DetailExample example);

    int updateByPrimaryKeySelective(Detail record);

    int updateByPrimaryKey(Detail record);

    List<DetailDTO> getAllInfo();

    Integer getOrdersNum1();

    Integer getOrdersNum2();

    Integer getOrdersNum3();

    Integer getOrdersNum4();

    Integer getOrdersNum5();

    Integer getOrdersNum6();

    Integer getOrdersNum7();

}
