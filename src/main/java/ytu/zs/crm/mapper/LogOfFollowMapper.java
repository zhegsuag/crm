package ytu.zs.crm.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ytu.zs.crm.pojo.Follow;
import ytu.zs.crm.pojo.LogOfFollow;
import ytu.zs.crm.pojo.LogOfFollowExample;

import javax.annotation.Resource;

@Repository
public interface LogOfFollowMapper {
    long countByExample(LogOfFollowExample example);

    int deleteByExample(LogOfFollowExample example);

    int deleteByPrimaryKey(Integer lfId);

    int insert(Follow row);

    int insertSelective(Follow row);

    List<LogOfFollow> selectByExample(LogOfFollowExample example);

    LogOfFollow selectByPrimaryKey(Integer lfId);

    int updateByExampleSelective(@Param("row") LogOfFollow row, @Param("example") LogOfFollowExample example);

    int updateByExample(@Param("row") LogOfFollow row, @Param("example") LogOfFollowExample example);

    int updateByPrimaryKeySelective(LogOfFollow row);

    int updateByPrimaryKey(LogOfFollow row);

    List<LogOfFollow> selectByCid(Integer cid);

    Integer selectByUserName(String n);

}
