package ytu.zs.crm.mapper;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ytu.zs.crm.pojo.Follow;
import ytu.zs.crm.pojo.FollowExample;

@Repository
public interface FollowMapper {
    long countByExample(FollowExample example);

    int deleteByExample(FollowExample example);

    int deleteByPrimaryKey(Integer fid);

    int insert(Follow row);

    int insertSelective(Follow row);

    List<Follow> selectByExample(FollowExample example);

    Follow selectByPrimaryKey(Integer fid);

    int updateByExampleSelective(@Param("row") Follow row, @Param("example") FollowExample example);

    int updateByExample(@Param("row") Follow row, @Param("example") FollowExample example);

    int updateByPrimaryKeySelective(Follow row);

    int updateByPrimaryKey(Follow row);

    List<Follow> selectByPading(String username);

    List<Follow> selectByCid(Integer cid);

    List<Follow> selectBacklog(@Param("username") String username,@Param("date") Date date);
}
