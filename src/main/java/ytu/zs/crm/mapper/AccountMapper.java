package ytu.zs.crm.mapper;

import ytu.zs.crm.pojo.Account;
import ytu.zs.crm.pojo.AccountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountMapper {
    long countByExample(AccountExample example);

    int deleteByExample(AccountExample example);

    int deleteByPrimaryKey(Integer aId);

    int insert(Account record);

    int insertSelective(Account record);

    List<Account> selectByExample(AccountExample example);

    List<Account> selectoflogin(Account account);

    Account selectByPrimaryKey(Integer aId);

    int updateByExampleSelective(@Param("record") Account record, @Param("example") AccountExample example);

    int updateByExample(@Param("record") Account record, @Param("example") AccountExample example);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);

    Integer selectinformation(@Param("record")Account record);

    Account selectofname(String name);

    List<String> selectname();
}
