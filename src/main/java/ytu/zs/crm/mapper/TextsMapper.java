package ytu.zs.crm.mapper;

import ytu.zs.crm.controller.texts.dto.TextsDTO;
import ytu.zs.crm.pojo.Texts;
import ytu.zs.crm.pojo.TextsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TextsMapper {
    long countByExample(TextsExample example);

    int deleteByExample(TextsExample example);

    int deleteByPrimaryKey(Integer tid);

    int insert(Texts record);

    int insertText(TextsDTO textsDTO);

    int insertSelective(Texts record);

    List<Texts> selectByExample(TextsExample example);

    Texts selectByPrimaryKey(Integer tid);

    int updateByExampleSelective(@Param("record") Texts record, @Param("example") TextsExample example);

    int updateByExample(@Param("record") Texts record, @Param("example") TextsExample example);

    int updateByPrimaryKeySelective(Texts record);

    int updateByPrimaryKey(Texts record);

    List<TextsDTO> getInfo();
}
