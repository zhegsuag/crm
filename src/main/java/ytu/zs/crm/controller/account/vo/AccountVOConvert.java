package ytu.zs.crm.controller.account.vo;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ytu.zs.crm.pojo.Account;
import ytu.zs.crm.pojo.Company;

import java.util.List;

/**
 * @description: AccountVO转换
 * @author: zhegsuag
 */
@Mapper
public interface AccountVOConvert {

    AccountVOConvert INSTANCE = Mappers.getMapper(AccountVOConvert.class);

    @Mapping(target = "username", source = "username")
    @Mapping(target = "password", source = "password")
    @Mapping(target = "headImg", source = "headImg")
    @Mapping(target = "phone", source = "phone")
    @Mapping(target = "email", source = "email")
    AccountVO toVO(Account account);

    List<AccountVO> toVO(List<Account> accounts);
}
