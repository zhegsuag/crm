package ytu.zs.crm.controller.account.vo;

import lombok.Data;

/**
 * @description: 员工信息：AccountVO
 * @author: zhegsuag
 */
@Data
public class AccountVO {

    private String username;

    private String password;

    private String headImg;

    private String phone;

    private String email;

    private String code;
}
