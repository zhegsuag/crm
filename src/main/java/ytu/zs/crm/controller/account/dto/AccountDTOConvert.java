package ytu.zs.crm.controller.account.dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ytu.zs.crm.controller.account.vo.AccountVO;
import ytu.zs.crm.pojo.Account;

import java.util.List;

/**
 * @description: AccountDTO转换
 * @author: zhegsuag
 */
@Mapper
public interface AccountDTOConvert {

    AccountDTOConvert INSTANCE = Mappers.getMapper(AccountDTOConvert.class);

    @Mapping(target = "username", source = "username")
    @Mapping(target = "password", source = "password")
    @Mapping(target = "headImg", source = "headImg")
    @Mapping(target = "phone", source = "phone")
    @Mapping(target = "email", source = "email")
    Account toDTO(AccountVO accountVO);

    List<Account> toDTO(List<AccountVO> accountVOS);
}
