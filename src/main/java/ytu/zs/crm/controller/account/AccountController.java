package ytu.zs.crm.controller.account;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.common.utils.FileUtils;
import ytu.zs.crm.controller.orders.dto.OrdersDTOConVert;
import ytu.zs.crm.controller.orders.vo.OrdersVO;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.pojo.Account;
import ytu.zs.crm.pojo.Orders;
import ytu.zs.crm.service.AccountService;
import ytu.zs.crm.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @description: 账户信息
 * @author: zhegsuag
 */
@RestController

@RequestMapping("account")
@Api(tags = "账户信息")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/list")
    @ApiOperation(value = "sync",notes = "获取账户列表",httpMethod = "GET")
    @OperLog(title = "获取账户列表",businessType = BusinessType.SELECT)
    public MsgAction<?> getAllList(){
        List<Account> accounts = accountService.getAllList();
        Assert.notNull(accounts,"列表为空");
        return MsgAction.ok(accounts);
    }

    @PutMapping("/updataimg")
    @ApiOperation(value = "sync",notes = "更新头像",httpMethod = "POST")
    @OperLog(title = "更新头像",businessType = BusinessType.UPDATE)
    public MsgAction<?> updataImg(MultipartFile file,Integer id){
        String name = FileUtils.renameFile(file);
        String path = "D:\\360极速浏览器\\vue-crm\\static\\"+name;
        FileUtils.transferToFile(file,path);
        Account account = new Account();
        account.setaId(id);
        account.setHeadImg("\\static\\"+name);
        this.loginService.updatepersion(account);
        return MsgAction.ok(path);
    }

    @PostMapping("/selectinformation")
    @ApiOperation(value = "sync",notes = "用户信息是否重复",httpMethod = "POST")
    @OperLog(title = "用户信息是否重复",businessType = BusinessType.SELECT)
    public MsgAction<?> selectinformation(@Validated @RequestBody Account account){
        return this.loginService.selectinformation(account);
    }

    @GetMapping("/paging")
    @ApiOperation(value = "info", notes = "查看用户信息", httpMethod = "GET")
    @OperLog(title = "查看用户信息",businessType = BusinessType.SELECT)
    public MsgInter<?> pading(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息为空");
        PageInfo<?> info = accountService.paging(pageInfo);
        return MsgAction.ok(info);
    }

    @PostMapping("/sync")
    @ApiOperation(value = "sync", notes = "添加用户信息", httpMethod = "POST")
    @OperLog(title = "添加用户信息",businessType = BusinessType.INSERT)
    public MsgInter<?> addOrders(@RequestBody Account account) {
        Assert.notNull(account, "用户信息为空");
        return accountService.addAccount(account);
    }

    @PutMapping("/sync")
    @ApiOperation(value = "sync", notes = "更新用户信息", httpMethod = "PUT")
    @OperLog(title = "更新用户信息",businessType = BusinessType.UPDATE)
    public MsgInter<?> update(@RequestBody Account account) {
        Assert.notNull(account.getaId(), "id为空");
        return accountService.update(account);
    }

    @DeleteMapping("/sync/{id}")
    @ApiOperation(value = "sync", notes = "删除用户信息", httpMethod = "DELETE")
    @OperLog(title = "删除用户信息",businessType = BusinessType.DELETE)
    public MsgInter<?> delete(@PathVariable("id") Integer id) {
        Assert.notNull(id, "id为空");
        return accountService.deleteById(id);
    }
}
