package ytu.zs.crm.controller.account.dto;

import lombok.Data;

/**
 * @description: 员工信息：AccountDTO
 * @author: zhegsuag
 */
@Data
public class AccountDTO {

    private String username;

    private String password;

    private String headImg;

    private String phone;

    private String email;

    private String code;
}
