package ytu.zs.crm.controller.company;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.common.target.UserLoginToken;
import ytu.zs.crm.controller.company.dto.CompanyDTOConvert;
import ytu.zs.crm.controller.company.vo.CompanyVO;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.pojo.Company;
import ytu.zs.crm.service.CompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 公司信息
 * @author: zhegsuag
 */
@RestController
@RequestMapping("company")

@Api(tags = "公司信息")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @UserLoginToken
    @PostMapping("/sync")
    @ApiOperation(value = "sync",notes = "添加公司信息",httpMethod = "POST")
    @OperLog(title = "添加公司信息",businessType = BusinessType.INSERT)
    public MsgInter<?> addCompany(@RequestBody CompanyVO companyVO) {
        Assert.notNull(companyVO, "");
        Company company = CompanyDTOConvert.INSTANCE.toDTO(companyVO);
        return companyService.addCompany(company);
    }

    @UserLoginToken
    @PutMapping("/sync")
    @ApiOperation(value = "sync",notes = "更新公司信息",httpMethod = "PUT")
    @OperLog(title = "更新公司信息",businessType = BusinessType.UPDATE)
    public MsgInter<?> update(@RequestBody CompanyVO companyVO){
        Assert.notNull(companyVO.getId(), "id为空");
        Company company = CompanyDTOConvert.INSTANCE.toDTO(companyVO);
        return companyService.update(company);
    }

    @UserLoginToken
    @DeleteMapping("/sync/{id}")
    @ApiOperation(value = "sync",notes = "删除公司信息",httpMethod = "DELETE")
    @OperLog(title = "删除公司信息",businessType = BusinessType.DELETE)
    public MsgInter<?> delete(@PathVariable("id") Integer id){
        Assert.notNull(id,"id为空");
        return companyService.delete(id);
    }

    @GetMapping("/paging")
    @ApiOperation(value = "paging",notes = "获取公司分页列表",httpMethod = "GET")
    @OperLog(title = "获取公司分页列表",businessType = BusinessType.SELECT)
    public MsgInter<?> getCompanyInfo(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息为空");
        PageInfo<?> info = companyService.getPageInfo(pageInfo);
        return MsgAction.ok(info);
    }

    @GetMapping("/list")
    @ApiOperation(value = "list",notes = "获取公司列表",httpMethod = "GET")
    @OperLog(title = "获取公司列表",businessType = BusinessType.SELECT)
    public MsgInter<?> getCompanyAll() {
        List<Company> allList = companyService.getAllList();
        return MsgAction.ok(allList);
    }
}
