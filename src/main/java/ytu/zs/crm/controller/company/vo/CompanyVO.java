package ytu.zs.crm.controller.company.vo;

import lombok.Data;

/**
 * @description: 公司信息：CompanyVO
 * @author: zhegsuag
 */
@Data
public class CompanyVO {
    private Integer id;       //公司id
    private String  name;     //公司名称
    private String  email;    //公司邮箱
    private String  phone;    //公司电话
    private String  address;  //公司地址
    private String  website;  //公司官网
}
