package ytu.zs.crm.controller.company.dto;

import lombok.Data;

/**
 * @description: 公司信息：CompanyDTO
 * @author: zhegsuag
 */
@Data
public class CompanyDTO {
    private Integer cpid;       //公司id
    private String  cpName;     //公司名称
    private String  cpEmail;    //公司邮箱
    private String  cpPhone;    //公司电话
    private String  cpAddress;  //公司地址
    private String  cpWebsite;  //公司官网
}
