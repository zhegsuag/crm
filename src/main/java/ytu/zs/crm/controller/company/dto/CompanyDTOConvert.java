package ytu.zs.crm.controller.company.dto;

import ytu.zs.crm.controller.company.vo.CompanyVO;
import ytu.zs.crm.pojo.Company;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description: CompanyDTO转换
 * @author: zhegsuag
 */
@Mapper
public interface CompanyDTOConvert {

    CompanyDTOConvert INSTANCE = Mappers.getMapper(CompanyDTOConvert.class);


    @Mapping(target = "cpid",source = "id")
    @Mapping(target = "cpName",source = "name")
    @Mapping(target = "cpEmail",source = "email")
    @Mapping(target = "cpPhone",source = "phone")
    @Mapping(target = "cpAddress",source = "address")
    @Mapping(target = "cpWebsite",source = "website")
    Company toDTO(CompanyVO companyVO);

    List<Company> toDTO(List<CompanyVO> companyVOS);
}
