package ytu.zs.crm.controller.company.vo;

import ytu.zs.crm.pojo.Company;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description: CompanyVO转换
 * @author: zhegsuag
 */
@Mapper
public interface CompanyVOConvert {

    CompanyVOConvert INSTANCE = Mappers.getMapper(CompanyVOConvert.class);

    @Mapping(target = "id",source = "cpid")
    @Mapping(target = "name",source = "cpName")
    @Mapping(target = "email",source = "cpEmail")
    @Mapping(target = "phone",source = "cpPhone")
    @Mapping(target = "address",source = "cpAddress")
    @Mapping(target = "website",source = "cpWebsite")
    CompanyVO toVO(Company company);

    List<CompanyVO>  toVO(List<Company> companies);
}
