package ytu.zs.crm.controller.wares.dto;

import ytu.zs.crm.controller.wares.vo.WaresVO;
import ytu.zs.crm.pojo.Wares;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @description:
 * @author: zhegsuag
 */
@Mapper
public interface WaresDTOConvert {
    WaresDTOConvert INSTANCE = Mappers.getMapper(WaresDTOConvert.class);

    @Mapping(source = "name",target = "wName")
    @Mapping(target = "wid",ignore = true)
    @Mapping(target = "wStock",ignore = true)
    @Mapping(target = "wPrice",ignore = true)
    Wares toDTO(WaresVO waresVO);

}
