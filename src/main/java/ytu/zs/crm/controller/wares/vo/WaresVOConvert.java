package ytu.zs.crm.controller.wares.vo;

import ytu.zs.crm.pojo.Wares;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description:
 * @author: zhegsuag
 */
@Mapper
public interface WaresVOConvert {

    WaresVOConvert INSTANCE = Mappers.getMapper(WaresVOConvert.class);

    @Mapping(source = "wName",target = "name")
    WaresVO toVO(Wares wares);

    List<WaresVO> toVO(List<Wares> wares);
}
