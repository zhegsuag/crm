package ytu.zs.crm.controller.wares;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.pojo.Wares;
import ytu.zs.crm.service.WaresService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * @description: 商品类别
 * @author: zhegsuag
 */

@RestController
@RequestMapping("wares")
@Api(tags = "商品种类")
public class WaresController {

    @Autowired
    private WaresService waresService;

    @PostMapping("/sync")
    @ApiOperation(value = "sync",notes = "添加商品种类",httpMethod = "POST")
    @OperLog(title = "添加商品种类",businessType = BusinessType.INSERT)
    public MsgInter<?> addWares(@RequestBody Wares wares){
        Assert.notNull(wares,"");
       return waresService.addWares(wares);
    }

    @PutMapping("/sync")
    @ApiOperation(value = "sync",notes = "修改商品种类信息",httpMethod = "PUT")
    @OperLog(title = "修改商品种类信息",businessType = BusinessType.UPDATE)
    public MsgInter<?> update(@RequestBody Wares wares){
        Assert.notNull(wares,"");
        return waresService.update(wares);
    }

    @DeleteMapping("/sync/{id}")
    @ApiOperation(value = "sync",notes = "删除商品种类",httpMethod = "DELETE")
    @OperLog(title = "删除商品种类",businessType = BusinessType.DELETE)
    public MsgInter<?> delete(@PathVariable("id") Integer id){
        Assert.notNull(id,"id为空");
        return waresService.delete(id);

    }

    @GetMapping("/info")
    @ApiOperation(value = "info",notes = "获取商品列表",httpMethod = "GET")
    @OperLog(title = "获取商品列表",businessType = BusinessType.SELECT)
    public MsgInter<?> getWaresInfo(){
        return waresService.getWaresInfo();
    }

    @GetMapping("/info_name/{name}")
    @ApiOperation(value = "info_name",notes = "获取某个商品信息",httpMethod = "GET")
    @OperLog(title = "获取某个商品信息",businessType = BusinessType.SELECT)
    public MsgInter<?> getWaresByName(@PathVariable String name){
        Assert.notNull(name,"");
        return waresService.getWaresByName(name);
    }

    @GetMapping("/paging")
    @ApiOperation(value = "paging",notes = "获取商品分页列表",httpMethod = "GET")
    @OperLog(title = "获取商品分页列表",businessType = BusinessType.SELECT)
    public MsgInter<?> getPageInfo(PageInfo<?> pageInfo){
        Assert.notNull(pageInfo,"分页信息为空");
        PageInfo<?> info = waresService.getPageInfo(pageInfo);
        return MsgAction.ok(info);
    }

}
