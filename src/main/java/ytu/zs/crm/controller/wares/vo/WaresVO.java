package ytu.zs.crm.controller.wares.vo;

import lombok.Data;

/**
 * @description:
 * @author: zhegsuag
 */
@Data
public class WaresVO {
    private String name;
}
