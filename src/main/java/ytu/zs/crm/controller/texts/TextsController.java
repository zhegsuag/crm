package ytu.zs.crm.controller.texts;

import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.texts.dto.TextsDTOConvert;
import ytu.zs.crm.controller.texts.vo.TextsVO;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.pojo.Texts;
import ytu.zs.crm.service.TextsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * @description: 待办事项
 * @author: zhegsuag
 */

@RestController
@RequestMapping("texts")
public class TextsController {

    @Autowired
    private TextsService textsService;

    @GetMapping("info")
    @OperLog(title = "获取待办",businessType = BusinessType.SELECT)
    public MsgInter<?> getInfo() {
        return textsService.getInfo();
    }

    @PostMapping("sync")
    @OperLog(title = "添加待办",businessType = BusinessType.INSERT)
    public MsgInter<?> addText(@RequestBody TextsVO textsVO) {
        Assert.notNull(textsVO.getText(), "待办事项为空");
        Texts texts = TextsDTOConvert.INSTANCE.toDTO(textsVO);
        return textsService.addText(texts);
    }

    @PutMapping("sync")
    @OperLog(title = "更新待办",businessType = BusinessType.UPDATE)
    public MsgInter<?> update(@RequestBody TextsVO textsVO) {
        Assert.notNull(textsVO.getTid(), "事项为空");
        return textsService.update(textsVO);
    }
}
