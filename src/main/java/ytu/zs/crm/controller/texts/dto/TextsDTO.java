package ytu.zs.crm.controller.texts.dto;

import ytu.zs.crm.pojo.Orders;
import lombok.Data;

/**
 * @description:
 * @author: zhegsuag
 */
@Data
public class TextsDTO {
    private Integer tid;
    private String  text;
    private Orders  oid;
    private Byte    finish;
}
