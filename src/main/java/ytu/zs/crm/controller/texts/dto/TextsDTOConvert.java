package ytu.zs.crm.controller.texts.dto;

import ytu.zs.crm.controller.texts.vo.TextsVO;
import ytu.zs.crm.pojo.Texts;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description: 转换成DTO
 * @author: zhegsuag
 */
@Mapper
public interface TextsDTOConvert {
    TextsDTOConvert INSTANCE = Mappers.getMapper(TextsDTOConvert.class);

    @Mapping(target = "finish", ignore = true)
    @Mapping(target = "oid",ignore = true)
    Texts toDTO(TextsVO textsVO);

    List<Texts> toDTO(List<TextsVO> textsVOS);
}
