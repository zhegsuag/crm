package ytu.zs.crm.controller.texts.vo;

import ytu.zs.crm.controller.texts.dto.TextsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description: 转换成VO
 * @author: zhegsuag
 */
@Mapper
public interface TextsVOConvert {
    TextsVOConvert INSTANCE= Mappers.getMapper(TextsVOConvert.class);

    @Mapping(target = "finish",ignore = true)
    TextsVO toVO(TextsDTO textsDTO);

    List<TextsVO> toVO(List<TextsDTO> textsDTOS);
}
