package ytu.zs.crm.controller.texts.vo;

import ytu.zs.crm.pojo.Orders;
import lombok.Data;

/**
 * @description: 待办事项VO
 * @author: zhegsuag
 */
@Data
public class TextsVO {
    private Integer tid;
    private String  text;
    private Orders  oid;
    private boolean  finish;
}
