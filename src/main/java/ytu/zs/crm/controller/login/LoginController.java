package ytu.zs.crm.controller.login;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.generator.MathGenerator;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.target.PassToken;
import ytu.zs.crm.common.target.UserLoginToken;
import ytu.zs.crm.controller.login.vo.AccountLoginVO;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.mapper.AccountMapper;
import ytu.zs.crm.pojo.Account;
import ytu.zs.crm.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ytu.zs.crm.service.TokenService;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description: 客户操作
 * @author: zhegsuag
 */
@RestController
@RequestMapping(value = "user_login")
@Api(tags = "客户操作")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private AccountMapper loginmapper;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private HttpServletResponse response;

    @PassToken
    @ApiOperation(value = "login",notes = "用戶登陆",httpMethod = "POST")
    @PostMapping(value = "/login")
    @OperLog(title = "用戶登陆",businessType = BusinessType.SELECT)
    public MsgInter<String> login(@Validated @RequestBody AccountLoginVO accountVO){
        accountVO.verifyThrow();
        MsgAction<String> msgAction = this.loginService.login(accountVO.getUsername(),accountVO.getPassword());
        if ("ok".equals(msgAction.getCode())){
            response.addHeader("Authorization",tokenService.getToken(new Account(accountVO.getUsername(),accountVO.getPassword())));
        }
        return msgAction;
    }

    @OperLog(title = "用戶信息",businessType = BusinessType.SELECT)
    @ApiOperation(value = "persion",notes = "用戶信息",httpMethod = "POST")
    @PostMapping(value = "/persion")
    public MsgInter<Account> persion(@Validated @RequestBody AccountLoginVO accountVO){
        accountVO.verifyThrow();
        return  this.loginService.persion(accountVO.getUsername(),accountVO.getPassword());
    }

    @ApiOperation(value = "updatepersion",notes = "修改客户信息",httpMethod = "POST")
    @PostMapping(value = "/updatepersion")
    @OperLog(title = "修改客户信息",businessType = BusinessType.UPDATE)
    public MsgInter<Account> updatepersion(@Validated @RequestBody Account account){
        return this.loginService.updatepersion(account);
    }

    @RequestMapping("/code")
    @OperLog(title = "获取验证码",businessType = BusinessType.SELECT)
    public void code(HttpServletResponse response) throws Exception {
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(80,32,4,40);
        lineCaptcha.setGenerator(new MathGenerator(1));
        lineCaptcha.createCode();
        Account account = new Account();
        account.setaId(1);
        account.setCode(lineCaptcha.getCode());
        lineCaptcha.write(response.getOutputStream());
        loginService.updatepersion(account);
    }

    @RequestMapping("/verify")
    @OperLog(title = "验证验证码是否正确",businessType = BusinessType.SELECT)
    public boolean verify(HttpServletRequest request,String code) throws Exception {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        String codesum = loginmapper.selectByPrimaryKey(1).getCode();
        return engine.eval(codesum.substring(0,codesum.length()-1))==Integer.valueOf(code);
    }
}
