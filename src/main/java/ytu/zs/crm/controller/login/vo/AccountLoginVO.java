package ytu.zs.crm.controller.login.vo;

import ytu.zs.crm.common.verify.Verify;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @Description: Account实体VO
 * @author: zhegsuag
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AccountLoginVO implements Verify {

    @NotBlank(message = "用户名不为空")
    @Length(max = 32, message = "用户名长度最大为32")
    private String username;

    @NotBlank(message = "密码不为空")
    @Length(max = 18, min = 5, message = "密码长度为5-18")
    private String password;

}
