package ytu.zs.crm.controller.follow;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.cust.vo.CustomerVO;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.pojo.Follow;
import ytu.zs.crm.pojo.LogOfFollow;
import ytu.zs.crm.service.FollowService;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: FollowController
 * @Author: zhegsuag
 * @Version: 1.0
 **/

@Api(tags = "跟进客户信息操作")
@RestController
@RequestMapping(value = "follow")
public class FollowController {

    @Autowired
    private FollowService followService;

    @GetMapping("/paging/{username}")
    @ApiOperation(value = "paging", notes = "获取客户跟进分页列表", httpMethod = "GET")
    @OperLog(title = "获取客户分页列表",businessType = BusinessType.SELECT)
    public MsgInter<?> getFollowPageInfo(PageInfo<?> pageInfo, @PathVariable("username") String username) {
        /*需求：分页显示  PageInfo pageInfo*/
        PageInfo<Follow> info = followService.getFollowPageInfo(pageInfo,username);
        return MsgAction.ok(info);
    }

    @PostMapping("/history")
    @ApiOperation(value = "history", notes = "获取跟进客户的历史列表", httpMethod = "POST")
    @OperLog(title = "获取跟进客户的历史列表",businessType = BusinessType.SELECT)
    public MsgInter<?> history(@RequestBody Follow follow) {
        List<LogOfFollow> followList = followService.getHistory(follow.getCid());
        return MsgAction.ok(followList);
    }

    @PostMapping("/update")
    @ApiOperation(value = "update", notes = "更新跟进客户的信息", httpMethod = "POST")
    @OperLog(title = "更新跟进客户的信息",businessType = BusinessType.UPDATE)
    public MsgInter<?> update(@RequestBody Follow follow) {
        return MsgAction.ok(followService.update(follow));
    }

    @PostMapping("/delete")
    @ApiOperation(value = "delete", notes = "更新跟进客户的信息", httpMethod = "DELETE")
    @OperLog(title = "更新跟进客户的信息",businessType = BusinessType.DELETE)
    public MsgInter<?> delete(@RequestBody Follow follow) {
        return followService.delete(follow);
    }

    @PostMapping("/backlog/{username}/{date}")
    @ApiOperation(value = "backlog", notes = "待办事项", httpMethod = "POST")
    @OperLog(title = "待办事项",businessType = BusinessType.SELECT)
    public MsgInter<?> backlog(@PathVariable("username") String username,@PathVariable("date") Date date) {
        return MsgAction.ok(followService.backlog(username,date));
    }

    @PostMapping("/echart2")
    @ApiOperation(value = "echart2", notes = "业务统计图表", httpMethod = "POST")
    @OperLog(title = "业务统计图表",businessType = BusinessType.SELECT)
    public MsgInter<?> Echart2() {
        return MsgAction.ok(followService.Echart2());
    }
}
