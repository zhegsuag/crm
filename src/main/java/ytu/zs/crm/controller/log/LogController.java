package ytu.zs.crm.controller.log;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.log.pojo.SysOperLog;
import ytu.zs.crm.service.LogService;

import java.util.List;

/**
 * @ClassName: LogController
 * @Description: 查找操作日志
 * @Author: zhegsuag
 **/

@RestController
@RequestMapping("/log")
public class LogController {

    @Autowired
    private LogService logService;

    @GetMapping("/info")
    @ApiOperation(value = "info", notes = "查看操作日志", httpMethod = "GET")
    @OperLog(title = "查看操作日志",businessType = BusinessType.SELECT)
    public MsgInter<?> loginfo(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息为空");
        PageInfo<?> info = logService.selectOperLog(pageInfo);
        return MsgAction.ok(info);
    }

    @GetMapping("export")
    @ApiOperation(value = "export", notes = "导出日志", httpMethod = "EXPORT")
    @OperLog(title = "导出日志", businessType = BusinessType.EXPORT)
    public void export() {
        logService.excel();
    }

}
