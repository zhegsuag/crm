package ytu.zs.crm.controller.message;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.login.vo.AccountLoginVO;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.pojo.Account;
import ytu.zs.crm.service.MessageService;

import javax.websocket.server.PathParam;

/**
 * @ClassName: MessageController
 * @Description: TODO
 * @Author: zhegsuag
 * @Date: 2022/4/20 14:14
 * @Version: 1.0
 **/
@RestController
@RequestMapping(value = "message")
@Api(tags = "消息提醒")
public class MessageController {

    @Autowired
    MessageService messageService;

    @ApiOperation(value = "pading",notes = "获取消息",httpMethod = "POST")
    @PostMapping(value = "/pading/{state}")
    @OperLog(title = "获取消息",businessType = BusinessType.SELECT)
    public MsgInter<?> pading(@PathVariable("state") Integer state){
        return MsgAction.ok(this.messageService.pading(state));
    }

    @ApiOperation(value = "sync",notes = "获取消息",httpMethod = "POST")
    @PostMapping(value = "/sync/{mid}/{state}")
    @OperLog(title = "获取消息",businessType = BusinessType.SELECT)
    public MsgInter<?> sync(@PathVariable("mid") Integer mid,@PathVariable("state")Integer state){
        return this.messageService.sync(mid,state);
    }

}
