package ytu.zs.crm.controller.cust;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.pojo.StateInfo;
import ytu.zs.crm.service.StateInfoService;
import ytu.zs.crm.service.StateService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: StateInfoController
 * @Description: TODO
 * @Author: zhegsuag
 * @Date: 2022/5/21 18:20
 * @Version: 1.0
 **/
@RestController
@RequestMapping("/stateinfo")
public class StateInfoController {

    @Autowired
    private StateInfoService stateinfoService;

    @GetMapping("/info")
    @OperLog(title = "查询状态信息",businessType = BusinessType.SELECT)
    public MsgInter<?> getStateInfo() {
        return stateinfoService.getStateInfo();
    }

    @PostMapping("/get")
    @OperLog(title = "更改状态信息",businessType = BusinessType.SELECT)
    public MsgInter<?> get(@RequestBody List<StateInfo> domains) {
        return stateinfoService.get(domains);
    }
}
