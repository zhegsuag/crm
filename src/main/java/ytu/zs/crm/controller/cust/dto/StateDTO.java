package ytu.zs.crm.controller.cust.dto;

import lombok.Data;

/**
 * @description: 客户状态DTO
 * @author: zhegsuag
 */
@Data
public class StateDTO {
    private String headImg;
    private String bgColor;

    private CustomerDTO customerDTO;
}
