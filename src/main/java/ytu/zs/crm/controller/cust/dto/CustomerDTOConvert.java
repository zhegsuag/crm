package ytu.zs.crm.controller.cust.dto;

import ytu.zs.crm.controller.cust.vo.CustomerVO;
import ytu.zs.crm.pojo.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @description: VO转换
 * @author: zhegsuag
 */
@Mapper
public interface CustomerDTOConvert {

    CustomerDTOConvert INSTANCE = Mappers.getMapper(CustomerDTOConvert.class);

    @Mapping(target = "cid", source = "id")
    @Mapping(target = "cName", source = "name")
    @Mapping(target = "cSex", source = "sex")
    @Mapping(target = "cAge", source = "age")
    @Mapping(target = "cPhone", source = "phone")
    @Mapping(target = "cEmail", source = "email")
    @Mapping(target = "cAddress", source = "address")
    @Mapping(target = "cBirth", source = "birth")
    @Mapping(target = "cpName", source = "company")
    @Mapping(target = "cUserName",source = "username")
    Customer toDTO(CustomerVO customerVO);
}
