package ytu.zs.crm.controller.cust.vo;

import ytu.zs.crm.controller.cust.dto.CustomerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @description:
 * @author: zhegsuag
 */
@Mapper
public interface CustVOConvert {
    CustVOConvert INSTANCE = Mappers.getMapper(CustVOConvert.class);

    @Mapping(target = "name",source = "cName")
    @Mapping(target = "phone",source = "cPhone")
    @Mapping(target = "email",source = "cEmail")
    @Mapping(target = "address",source = "cAddress")
    CustVO toVO(CustomerDTO customerDTO);
}
