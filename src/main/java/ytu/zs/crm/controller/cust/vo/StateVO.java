package ytu.zs.crm.controller.cust.vo;

import lombok.Data;

/**
 * @description: 客户状态VO
 * @author: zhegsuag
 */
@Data
public class StateVO {
    private String headImg;
    private String bgColor;

    private CustVO users;

}
