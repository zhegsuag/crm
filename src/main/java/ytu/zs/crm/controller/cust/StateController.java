package ytu.zs.crm.controller.cust;

import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.service.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @description: 客户状态
 * @author: zhegsuag
 */
@RestController

@RequestMapping("/state")
public class StateController {

    @Autowired
    private StateService stateService;

    @Resource
    private HttpServletRequest request;

    @GetMapping("/info")
    @OperLog(title = "获取危险，脱离客户状态信息",businessType = BusinessType.SELECT)
    public MsgInter<?> getStateInfo() {
        String userName = request.getHeader("username");
        if ("admin".equals(userName)){
            userName = null;
        }
        return stateService.getStateInfo(userName);
    }

    @GetMapping("/custInfo")
    @OperLog(title = "工作台危险，脱离客户状态数量",businessType = BusinessType.SELECT)
    public MsgInter<?> getCustInfo() {
        return stateService.getCustInfo();
    }
}

