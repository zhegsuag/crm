package ytu.zs.crm.controller.cust.vo;

import lombok.Data;

import java.util.Date;

/**
 * @description: CustomerVO
 * @author: zhegsuag
 */
@Data
public class CustomerVO {
    private Integer id;//id
    private String  name;//客户名称
    private String  sex;//性别
    private Integer age;//年龄
    private String  phone;//电话
    private String  email;//邮箱
    private String  address;//地址
    private Date    birth;//出生年月
    private String  company;//公司
    private String  username; //负责人
    private Integer total;//订单总数
    private String state;//客户状态

}
