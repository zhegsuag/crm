package ytu.zs.crm.controller.cust.vo;

import ytu.zs.crm.controller.cust.dto.CustomerDTO;
import ytu.zs.crm.pojo.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description: 转换VO
 * @author: zhegsuag
 */
@Mapper
public interface CustomerVOConvert {

    CustomerVOConvert INSTANCE = Mappers.getMapper(CustomerVOConvert.class);

    @Mapping(source = "cid", target = "id")
    @Mapping(source = "cName", target = "name")
    @Mapping(source = "cSex", target = "sex")
    @Mapping(source = "cAge", target = "age")
    @Mapping(source = "cPhone", target = "phone")
    @Mapping(source = "cEmail", target = "email")
    @Mapping(source = "cAddress", target = "address")
    @Mapping(source = "cBirth", target = "birth")
    @Mapping(source = "cpName", target = "company")
    @Mapping(target = "state",ignore = true)
    @Mapping(target = "total",ignore = true)
    @Mapping(target = "username",source = "cUserName")
    CustomerVO toVO(Customer customer);

    List<CustomerVO> toVO(List<Customer> customers);

    @Mapping(target = "id",source = "cid")
    @Mapping(target = "name",source = "cName")
    @Mapping(target = "phone",source = "cPhone")
    @Mapping(target = "email",source = "cEmail")
    @Mapping(target = "address",source = "cAddress")
    @Mapping(target = "username",source = "cUserName")
    @Mapping(target = "sex",ignore = true)
    @Mapping(target = "age",ignore = true)
    @Mapping(target = "company",ignore = true)
    @Mapping(target = "birth",ignore = true)
    CustomerVO toVO(CustomerDTO customerDTO);

    List<CustomerVO> toVOS(List<CustomerDTO> customerDTOS);

}
