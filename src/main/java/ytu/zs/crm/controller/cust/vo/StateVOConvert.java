package ytu.zs.crm.controller.cust.vo;

import ytu.zs.crm.controller.cust.dto.StateDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description: 转换成VO
 * @author: zhegsuag
 */
@Mapper(uses = CustVOConvert.class)
public interface StateVOConvert {
    StateVOConvert INSTANCE= Mappers.getMapper(StateVOConvert.class);

    @Mapping(target = "users",source = "customerDTO")
    StateVO toVO(StateDTO stateDTO);

    List<StateVO> toVO(List<StateDTO> stateDTOS);
}
