package ytu.zs.crm.controller.cust.vo;

import lombok.Data;

/**
 * @description:
 * @author: zhegsuag
 */
@Data
public class CustVO {
    private String name;
    private String phone;
    private String email;
    private String address;
    private Integer total;
    private String state;

}
