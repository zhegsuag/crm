package ytu.zs.crm.controller.cust;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.controller.cust.dto.CustomerDTOConvert;
import ytu.zs.crm.controller.cust.vo.CustomerVO;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.pojo.Customer;
import ytu.zs.crm.pojo.Follow;
import ytu.zs.crm.service.CustInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @description:
 * @author: zhegsuag
 * */

@Api(tags = "客户信息操作")
@RestController

@RequestMapping(value = "cust")
public class CustInfoController {

    @Autowired
    private CustInfoService custInfoService;

    @GetMapping("/info")
    @ApiOperation(value = "info", notes = "获取客户列表", httpMethod = "GET")
    @OperLog(title = "获取客户列表",businessType = BusinessType.SELECT)
    public MsgInter<?> custInfo() {
        List<CustomerVO> customerVOS = custInfoService.queryCustInfo();
        return MsgAction.ok(customerVOS);
    }


    @GetMapping("/info_name/{name}")
    @ApiOperation(value = "info_name", notes = "获取某个用户", httpMethod = "GET")
    @OperLog(title = "获取某个用户",businessType = BusinessType.SELECT)
    public MsgInter<?> getCustByName(@PathVariable String name) {
        Assert.notNull(name, "查询为空");
        return custInfoService.getCustByName(name);
    }

    @GetMapping("/paging")
    @ApiOperation(value = "paging", notes = "获取客户分页列表", httpMethod = "GET")
    @OperLog(title = "获取客户分页列表",businessType = BusinessType.SELECT)
    public MsgInter<?> getPageInfo(PageInfo<?> pageInfo, HttpServletRequest request) {
        /*需求：分页显示  PageInfo pageInfo*/
        PageInfo<CustomerVO> info = custInfoService.getPageInfo(pageInfo,request);
        return MsgAction.ok(info);
    }

    @PutMapping("/sync")
    @ApiOperation(value = "sync", notes = "更新客户信息", httpMethod = "PUT")
    @OperLog(title = "更新客户信息",businessType = BusinessType.UPDATE)
    public MsgInter<?> update(@RequestBody CustomerVO customerVO) {
        Assert.notNull(customerVO.getId(), "修改信息不存在!");
        Customer customer = CustomerDTOConvert.INSTANCE.toDTO(customerVO);
        return custInfoService.updata(customer);
    }

    @PostMapping("/syncOfSea/{cid}/{fid}")
    @ApiOperation(value = "syncOfSea", notes = "客户回退公海", httpMethod = "POST")
    @OperLog(title = "客户回退公海",businessType = BusinessType.UPDATE)
    public MsgInter<?> updateOfSea(@PathVariable("cid") Integer cid,@PathVariable("fid") Integer fid) {
        Assert.notNull(cid, "修改信息不存在!");
        return custInfoService.updateOfSea(cid,fid);
    }

    @PostMapping("/sync")
    @ApiOperation(value = "sync", notes = "添加客户信息", httpMethod = "POST")
    @OperLog(title = "添加客户信息",businessType = BusinessType.INSERT)
    public MsgInter<?> addCust(@RequestBody CustomerVO customerVO) {
        Assert.notNull(customerVO, "添加内容为空");
        Customer customer = CustomerDTOConvert.INSTANCE.toDTO(customerVO);
        return custInfoService.add(customer);
    }

    @DeleteMapping("/sync/{id}")
    @ApiOperation(value = "sync", notes = "删除客户信息", httpMethod = "DELETE")
    @OperLog(title = "删除客户信息",businessType = BusinessType.DELETE)
    public MsgInter<?> delete(@PathVariable("id") Integer id) {
        Assert.notNull(id, "id为空");
        return custInfoService.deleteById(id);
    }

}
