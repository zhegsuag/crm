package ytu.zs.crm.controller.record;

import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 最新消息
 * @author: zhegsuag
 */

@RestController
@RequestMapping("record")
public class RecordController {

    @Autowired
    private RecordService recordService;

    @GetMapping("info")
    @OperLog(title = "获取最新消息",businessType = BusinessType.SELECT)
    public MsgInter<?> getInfo() {
        return recordService.getInfo();
    }

}
