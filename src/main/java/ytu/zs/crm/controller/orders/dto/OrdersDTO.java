package ytu.zs.crm.controller.orders.dto;

import lombok.Data;

/**
 * @description: OrderDTO
 * @author: zhegsuag
 */
@Data
public class OrdersDTO {
    private Integer oid;//id
    private String  oName;//菜单名称
    private Integer number; //订购数量
    private String  consignee;//客户名称
    private String  address;//地址
    private String  phone;//联系电话
    private String  cust;//下单客户
    private Double  total;

}
