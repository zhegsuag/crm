package ytu.zs.crm.controller.orders;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.common.target.UserLoginToken;
import ytu.zs.crm.controller.orders.vo.DetailVO;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.service.DetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 订单明细处理
 * @author: zhegsuag
 */

@RestController
@RequestMapping("detail")
public class DetailController {

    @Autowired
    private DetailService detailService;

    @GetMapping("info")
    @OperLog(title = "获取已处理订单和未处理订单",businessType = BusinessType.SELECT)
    public MsgInter<?> getAllInfo() {
        List<DetailVO> info = detailService.getAllInfo();
        Assert.notNull(info, "");
        return MsgAction.ok(info);
    }

    @GetMapping("paging")
    @OperLog(title = "获取订单明细分页",businessType = BusinessType.SELECT)
    public MsgInter<?> paging(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息为空");
        PageInfo<?> paging = detailService.getPaging(pageInfo);
        return MsgAction.ok(paging);
    }

    @UserLoginToken
    @PutMapping("upState/{i}")
    @OperLog(title = "更新订单状态",businessType = BusinessType.UPDATE)
    public MsgInter<?> handleState(@RequestBody DetailVO detailVO, @PathVariable("i") @Nullable Integer i) {
        return detailService.handleState(detailVO, i);
    }

    @PostMapping("getNumber")
    @OperLog(title = "获取订单图表数据",businessType = BusinessType.SELECT)
    public MsgInter<?> getNumOfOrder() {
        return MsgAction.ok(detailService.getNumOfOrder());
    }

}
