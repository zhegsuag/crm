package ytu.zs.crm.controller.orders.vo;

import ytu.zs.crm.pojo.Orders;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description: OrdersVO转换
 * @author: zhegsuag
 */
@Mapper
public interface OrdersVOConvert {
    OrdersVOConvert INSTANCE = Mappers.getMapper(OrdersVOConvert.class);


    @Mapping(target = "id", source = "oid")
    @Mapping(target = "consignee",source = "consignee")
    @Mapping(target = "address",source = "address")
    @Mapping(target = "phone",source = "phone")
    @Mapping(target = "cust",source = "cust")
    @Mapping(target = "total",source = "total")
    OrdersVO toVO(Orders orders);

    List<OrdersVO> toVO(List<Orders> orders);
}
