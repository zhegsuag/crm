package ytu.zs.crm.controller.orders.vo;

import ytu.zs.crm.pojo.Orders;
import ytu.zs.crm.pojo.Wares;
import lombok.Data;

import java.util.Date;

/**
 * @description:
 * @author: zhegsuag
 */
@Data
public class DetailVO {
    private Wares  wid;
    private Orders oid;
    private Date   createTime;
    private  Date   handleTime;
    private  String state;
}
