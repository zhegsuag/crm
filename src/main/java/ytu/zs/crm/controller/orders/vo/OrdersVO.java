package ytu.zs.crm.controller.orders.vo;

import lombok.Data;
import ytu.zs.crm.pojo.Orders;

import java.util.List;

/**
 * @description:
 * @author: zhegsuag
 */
@Data
public class OrdersVO {

    private Integer id;//id
    private List<OrdersVO> orderlist;
    private String  name;//菜单名称
    private Integer number; //订购数量
    private String  consignee;//客户名称
    private String  address;//地址
    private String  phone;//联系电话
    private String  cust;//下单客户
    private Double  total;//订单总额


}
