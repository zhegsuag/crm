package ytu.zs.crm.controller.orders.dto;

import ytu.zs.crm.controller.orders.vo.OrdersVO;
import ytu.zs.crm.pojo.Orders;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description: OrdersDTO转换
 * @author: zhegsuag
 */
@Mapper
public interface OrdersDTOConVert {
    OrdersDTOConVert INSTANCE = Mappers.getMapper(OrdersDTOConVert.class);

    @Mapping(target = "oid", source = "id")
    @Mapping(target = "phone", source = "phone")
    @Mapping(target = "consignee", source = "consignee")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "cust", source = "cust")
    @Mapping(target = "total", source = "total")
    Orders toDTO(OrdersVO ordersVO);

    List<OrdersDTO> toDTO(List<OrdersVO> ordersVOS);
}
