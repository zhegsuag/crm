package ytu.zs.crm.controller.orders;

import com.github.pagehelper.PageInfo;
import ytu.zs.crm.common.msg.MsgAction;
import ytu.zs.crm.common.msg.MsgInter;
import ytu.zs.crm.common.target.UserLoginToken;
import ytu.zs.crm.controller.orders.dto.OrdersDTOConVert;
import ytu.zs.crm.controller.orders.vo.OrdersVO;
import ytu.zs.crm.log.annotation.OperLog;
import ytu.zs.crm.log.enums.BusinessType;
import ytu.zs.crm.pojo.Orders;
import ytu.zs.crm.service.OrdersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

/**
 * @description: 订单列表
 * @author: zhegsuag
 */

@RestController
@RequestMapping("orders")
@Api(tags = "订单列表")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @GetMapping("/info")
    @ApiOperation(value = "info", notes = "获取订单列表", httpMethod = "GET")
    @OperLog(title = "获取订单列表",businessType = BusinessType.SELECT)
    public MsgInter<?> orderInfo() {
        return ordersService.getOrdersInfo();
    }

    @GetMapping("/paging")
    @ApiOperation(value = "paging", notes = "获取订单分页列表", httpMethod = "GET")
    @OperLog(title = "获取订单分页列表",businessType = BusinessType.SELECT)
    public MsgInter<?> getPaging(PageInfo<?> pageInfo) {
        Assert.notNull(pageInfo, "分页信息为空");
        PageInfo<?> info = ordersService.getPaging(pageInfo);
        return MsgAction.ok(info);
    }

    @PostMapping("/sync")
    @ApiOperation(value = "sync", notes = "添加订单信息", httpMethod = "POST")
    @OperLog(title = "添加订单信息",businessType = BusinessType.INSERT)
    public MsgInter<?> addOrders(@RequestBody OrdersVO ordersVO) {
        Assert.notNull(ordersVO, "订单为空");
        Orders orders = OrdersDTOConVert.INSTANCE.toDTO(ordersVO);
        orders.setwName1(ordersVO.getOrderlist().get(0).getName());
        orders.setNumber1(ordersVO.getOrderlist().get(0).getNumber());
        if (ordersVO.getOrderlist().size() > 1) {
            orders.setwName2(ordersVO.getOrderlist().get(1).getName());
            orders.setNumber2(ordersVO.getOrderlist().get(1).getNumber());
        }
        if (ordersVO.getOrderlist().size() > 2) {
            orders.setwName3(ordersVO.getOrderlist().get(2).getName());
            orders.setNumber3(ordersVO.getOrderlist().get(2).getNumber());
        }
        if (ordersVO.getOrderlist().size() > 3) {
            orders.setwName4(ordersVO.getOrderlist().get(3).getName());
            orders.setNumber4(ordersVO.getOrderlist().get(3).getNumber());
        }
        if (ordersVO.getOrderlist().size() > 4) {
            orders.setwName5(ordersVO.getOrderlist().get(4).getName());
            orders.setNumber5(ordersVO.getOrderlist().get(4).getNumber());
        }
        return ordersService.addOrders(orders);
    }

    @PutMapping("/sync")
    @ApiOperation(value = "sync", notes = "修改订单信息", httpMethod = "PUT")
    @OperLog(title = "修改订单信息",businessType = BusinessType.UPDATE)
    public MsgInter<?> update(@RequestBody Orders ordersVO) {
        Assert.notNull(ordersVO.getOid(), "id为空");
        return ordersService.update(ordersVO);
    }

    @DeleteMapping("/sync/{id}")
    @ApiOperation(value = "sync", notes = "删除订单信息", httpMethod = "DELETE")
    @OperLog(title = "删除订单信息",businessType = BusinessType.DELETE)
    public MsgInter<?> delete(@PathVariable("id") Integer id) {
        Assert.notNull(id, "id为空");
        return ordersService.deleteById(id);
    }
}
