package ytu.zs.crm.controller.orders.dto;

import ytu.zs.crm.pojo.Orders;
import ytu.zs.crm.pojo.Wares;
import lombok.Data;

import java.util.Date;

/**
 * @description: 订单明细DTO
 * @author: zhegsuag
 */
@Data
public class DetailDTO {
    private Wares  wid;
    private Orders oid;
    private Date   createTime;
    private Date   handleTime;
    private Byte   state;
}
