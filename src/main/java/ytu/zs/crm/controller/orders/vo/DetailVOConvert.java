package ytu.zs.crm.controller.orders.vo;

import ytu.zs.crm.controller.orders.dto.DetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description: DetailDTO转换VO
 * @author: zhegsuag
 */
@Mapper
public interface DetailVOConvert {
    DetailVOConvert INSTANCE= Mappers.getMapper(DetailVOConvert.class);


    @Mapping(target = "state",ignore = true)
    @Mapping(target = "wid",source = "wid")
    @Mapping(target = "oid",source = "oid")
    @Mapping(target = "createTime",source = "createTime")
    @Mapping(target = "handleTime",source = "handleTime")
    DetailVO toVO(DetailDTO detailDTO);

    List<DetailVO> toVO(List<DetailDTO> detailDTOS);
}
