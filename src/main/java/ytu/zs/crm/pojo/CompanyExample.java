package ytu.zs.crm.pojo;

import java.util.ArrayList;
import java.util.List;

public class CompanyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CompanyExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCpidIsNull() {
            addCriterion("cpid is null");
            return (Criteria) this;
        }

        public Criteria andCpidIsNotNull() {
            addCriterion("cpid is not null");
            return (Criteria) this;
        }

        public Criteria andCpidEqualTo(Integer value) {
            addCriterion("cpid =", value, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpidNotEqualTo(Integer value) {
            addCriterion("cpid <>", value, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpidGreaterThan(Integer value) {
            addCriterion("cpid >", value, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpidGreaterThanOrEqualTo(Integer value) {
            addCriterion("cpid >=", value, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpidLessThan(Integer value) {
            addCriterion("cpid <", value, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpidLessThanOrEqualTo(Integer value) {
            addCriterion("cpid <=", value, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpidIn(List<Integer> values) {
            addCriterion("cpid in", values, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpidNotIn(List<Integer> values) {
            addCriterion("cpid not in", values, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpidBetween(Integer value1, Integer value2) {
            addCriterion("cpid between", value1, value2, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpidNotBetween(Integer value1, Integer value2) {
            addCriterion("cpid not between", value1, value2, "cpid");
            return (Criteria) this;
        }

        public Criteria andCpNameIsNull() {
            addCriterion("cp_name is null");
            return (Criteria) this;
        }

        public Criteria andCpNameIsNotNull() {
            addCriterion("cp_name is not null");
            return (Criteria) this;
        }

        public Criteria andCpNameEqualTo(String value) {
            addCriterion("cp_name =", value, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameNotEqualTo(String value) {
            addCriterion("cp_name <>", value, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameGreaterThan(String value) {
            addCriterion("cp_name >", value, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameGreaterThanOrEqualTo(String value) {
            addCriterion("cp_name >=", value, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameLessThan(String value) {
            addCriterion("cp_name <", value, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameLessThanOrEqualTo(String value) {
            addCriterion("cp_name <=", value, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameLike(String value) {
            addCriterion("cp_name like", value, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameNotLike(String value) {
            addCriterion("cp_name not like", value, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameIn(List<String> values) {
            addCriterion("cp_name in", values, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameNotIn(List<String> values) {
            addCriterion("cp_name not in", values, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameBetween(String value1, String value2) {
            addCriterion("cp_name between", value1, value2, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpNameNotBetween(String value1, String value2) {
            addCriterion("cp_name not between", value1, value2, "cpName");
            return (Criteria) this;
        }

        public Criteria andCpEmailIsNull() {
            addCriterion("cp_email is null");
            return (Criteria) this;
        }

        public Criteria andCpEmailIsNotNull() {
            addCriterion("cp_email is not null");
            return (Criteria) this;
        }

        public Criteria andCpEmailEqualTo(String value) {
            addCriterion("cp_email =", value, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailNotEqualTo(String value) {
            addCriterion("cp_email <>", value, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailGreaterThan(String value) {
            addCriterion("cp_email >", value, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailGreaterThanOrEqualTo(String value) {
            addCriterion("cp_email >=", value, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailLessThan(String value) {
            addCriterion("cp_email <", value, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailLessThanOrEqualTo(String value) {
            addCriterion("cp_email <=", value, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailLike(String value) {
            addCriterion("cp_email like", value, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailNotLike(String value) {
            addCriterion("cp_email not like", value, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailIn(List<String> values) {
            addCriterion("cp_email in", values, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailNotIn(List<String> values) {
            addCriterion("cp_email not in", values, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailBetween(String value1, String value2) {
            addCriterion("cp_email between", value1, value2, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpEmailNotBetween(String value1, String value2) {
            addCriterion("cp_email not between", value1, value2, "cpEmail");
            return (Criteria) this;
        }

        public Criteria andCpPhoneIsNull() {
            addCriterion("cp_phone is null");
            return (Criteria) this;
        }

        public Criteria andCpPhoneIsNotNull() {
            addCriterion("cp_phone is not null");
            return (Criteria) this;
        }

        public Criteria andCpPhoneEqualTo(String value) {
            addCriterion("cp_phone =", value, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneNotEqualTo(String value) {
            addCriterion("cp_phone <>", value, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneGreaterThan(String value) {
            addCriterion("cp_phone >", value, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("cp_phone >=", value, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneLessThan(String value) {
            addCriterion("cp_phone <", value, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneLessThanOrEqualTo(String value) {
            addCriterion("cp_phone <=", value, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneLike(String value) {
            addCriterion("cp_phone like", value, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneNotLike(String value) {
            addCriterion("cp_phone not like", value, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneIn(List<String> values) {
            addCriterion("cp_phone in", values, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneNotIn(List<String> values) {
            addCriterion("cp_phone not in", values, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneBetween(String value1, String value2) {
            addCriterion("cp_phone between", value1, value2, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpPhoneNotBetween(String value1, String value2) {
            addCriterion("cp_phone not between", value1, value2, "cpPhone");
            return (Criteria) this;
        }

        public Criteria andCpAddressIsNull() {
            addCriterion("cp_address is null");
            return (Criteria) this;
        }

        public Criteria andCpAddressIsNotNull() {
            addCriterion("cp_address is not null");
            return (Criteria) this;
        }

        public Criteria andCpAddressEqualTo(String value) {
            addCriterion("cp_address =", value, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressNotEqualTo(String value) {
            addCriterion("cp_address <>", value, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressGreaterThan(String value) {
            addCriterion("cp_address >", value, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressGreaterThanOrEqualTo(String value) {
            addCriterion("cp_address >=", value, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressLessThan(String value) {
            addCriterion("cp_address <", value, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressLessThanOrEqualTo(String value) {
            addCriterion("cp_address <=", value, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressLike(String value) {
            addCriterion("cp_address like", value, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressNotLike(String value) {
            addCriterion("cp_address not like", value, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressIn(List<String> values) {
            addCriterion("cp_address in", values, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressNotIn(List<String> values) {
            addCriterion("cp_address not in", values, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressBetween(String value1, String value2) {
            addCriterion("cp_address between", value1, value2, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpAddressNotBetween(String value1, String value2) {
            addCriterion("cp_address not between", value1, value2, "cpAddress");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteIsNull() {
            addCriterion("cp_website is null");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteIsNotNull() {
            addCriterion("cp_website is not null");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteEqualTo(String value) {
            addCriterion("cp_website =", value, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteNotEqualTo(String value) {
            addCriterion("cp_website <>", value, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteGreaterThan(String value) {
            addCriterion("cp_website >", value, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteGreaterThanOrEqualTo(String value) {
            addCriterion("cp_website >=", value, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteLessThan(String value) {
            addCriterion("cp_website <", value, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteLessThanOrEqualTo(String value) {
            addCriterion("cp_website <=", value, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteLike(String value) {
            addCriterion("cp_website like", value, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteNotLike(String value) {
            addCriterion("cp_website not like", value, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteIn(List<String> values) {
            addCriterion("cp_website in", values, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteNotIn(List<String> values) {
            addCriterion("cp_website not in", values, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteBetween(String value1, String value2) {
            addCriterion("cp_website between", value1, value2, "cpWebsite");
            return (Criteria) this;
        }

        public Criteria andCpWebsiteNotBetween(String value1, String value2) {
            addCriterion("cp_website not between", value1, value2, "cpWebsite");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
