package ytu.zs.crm.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FollowExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FollowExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andFidIsNull() {
            addCriterion("fid is null");
            return (Criteria) this;
        }

        public Criteria andFidIsNotNull() {
            addCriterion("fid is not null");
            return (Criteria) this;
        }

        public Criteria andFidEqualTo(Integer value) {
            addCriterion("fid =", value, "fid");
            return (Criteria) this;
        }

        public Criteria andFidNotEqualTo(Integer value) {
            addCriterion("fid <>", value, "fid");
            return (Criteria) this;
        }

        public Criteria andFidGreaterThan(Integer value) {
            addCriterion("fid >", value, "fid");
            return (Criteria) this;
        }

        public Criteria andFidGreaterThanOrEqualTo(Integer value) {
            addCriterion("fid >=", value, "fid");
            return (Criteria) this;
        }

        public Criteria andFidLessThan(Integer value) {
            addCriterion("fid <", value, "fid");
            return (Criteria) this;
        }

        public Criteria andFidLessThanOrEqualTo(Integer value) {
            addCriterion("fid <=", value, "fid");
            return (Criteria) this;
        }

        public Criteria andFidIn(List<Integer> values) {
            addCriterion("fid in", values, "fid");
            return (Criteria) this;
        }

        public Criteria andFidNotIn(List<Integer> values) {
            addCriterion("fid not in", values, "fid");
            return (Criteria) this;
        }

        public Criteria andFidBetween(Integer value1, Integer value2) {
            addCriterion("fid between", value1, value2, "fid");
            return (Criteria) this;
        }

        public Criteria andFidNotBetween(Integer value1, Integer value2) {
            addCriterion("fid not between", value1, value2, "fid");
            return (Criteria) this;
        }

        public Criteria andCidIsNull() {
            addCriterion("cid is null");
            return (Criteria) this;
        }

        public Criteria andCidIsNotNull() {
            addCriterion("cid is not null");
            return (Criteria) this;
        }

        public Criteria andCidEqualTo(Integer value) {
            addCriterion("cid =", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotEqualTo(Integer value) {
            addCriterion("cid <>", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThan(Integer value) {
            addCriterion("cid >", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThanOrEqualTo(Integer value) {
            addCriterion("cid >=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThan(Integer value) {
            addCriterion("cid <", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThanOrEqualTo(Integer value) {
            addCriterion("cid <=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidIn(List<Integer> values) {
            addCriterion("cid in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotIn(List<Integer> values) {
            addCriterion("cid not in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidBetween(Integer value1, Integer value2) {
            addCriterion("cid between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotBetween(Integer value1, Integer value2) {
            addCriterion("cid not between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andFCommunicateIsNull() {
            addCriterion("f_communicate is null");
            return (Criteria) this;
        }

        public Criteria andFCommunicateIsNotNull() {
            addCriterion("f_communicate is not null");
            return (Criteria) this;
        }

        public Criteria andFCommunicateEqualTo(String value) {
            addCriterion("f_communicate =", value, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateNotEqualTo(String value) {
            addCriterion("f_communicate <>", value, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateGreaterThan(String value) {
            addCriterion("f_communicate >", value, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateGreaterThanOrEqualTo(String value) {
            addCriterion("f_communicate >=", value, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateLessThan(String value) {
            addCriterion("f_communicate <", value, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateLessThanOrEqualTo(String value) {
            addCriterion("f_communicate <=", value, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateLike(String value) {
            addCriterion("f_communicate like", value, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateNotLike(String value) {
            addCriterion("f_communicate not like", value, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateIn(List<String> values) {
            addCriterion("f_communicate in", values, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateNotIn(List<String> values) {
            addCriterion("f_communicate not in", values, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateBetween(String value1, String value2) {
            addCriterion("f_communicate between", value1, value2, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFCommunicateNotBetween(String value1, String value2) {
            addCriterion("f_communicate not between", value1, value2, "fCommunicate");
            return (Criteria) this;
        }

        public Criteria andFDatatimeIsNull() {
            addCriterion("f_datatime is null");
            return (Criteria) this;
        }

        public Criteria andFDatatimeIsNotNull() {
            addCriterion("f_datatime is not null");
            return (Criteria) this;
        }

        public Criteria andFDatatimeEqualTo(Date value) {
            addCriterion("f_datatime =", value, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFDatatimeNotEqualTo(Date value) {
            addCriterion("f_datatime <>", value, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFDatatimeGreaterThan(Date value) {
            addCriterion("f_datatime >", value, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFDatatimeGreaterThanOrEqualTo(Date value) {
            addCriterion("f_datatime >=", value, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFDatatimeLessThan(Date value) {
            addCriterion("f_datatime <", value, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFDatatimeLessThanOrEqualTo(Date value) {
            addCriterion("f_datatime <=", value, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFDatatimeIn(List<Date> values) {
            addCriterion("f_datatime in", values, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFDatatimeNotIn(List<Date> values) {
            addCriterion("f_datatime not in", values, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFDatatimeBetween(Date value1, Date value2) {
            addCriterion("f_datatime between", value1, value2, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFDatatimeNotBetween(Date value1, Date value2) {
            addCriterion("f_datatime not between", value1, value2, "fDatatime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeIsNull() {
            addCriterion("f_nexttime is null");
            return (Criteria) this;
        }

        public Criteria andFNexttimeIsNotNull() {
            addCriterion("f_nexttime is not null");
            return (Criteria) this;
        }

        public Criteria andFNexttimeEqualTo(Date value) {
            addCriterion("f_nexttime =", value, "fNexttime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeNotEqualTo(Date value) {
            addCriterion("f_nexttime <>", value, "fNexttime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeGreaterThan(Date value) {
            addCriterion("f_nexttime >", value, "fNexttime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("f_nexttime >=", value, "fNexttime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeLessThan(Date value) {
            addCriterion("f_nexttime <", value, "fNexttime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeLessThanOrEqualTo(Date value) {
            addCriterion("f_nexttime <=", value, "fNexttime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeIn(List<Date> values) {
            addCriterion("f_nexttime in", values, "fNexttime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeNotIn(List<Date> values) {
            addCriterion("f_nexttime not in", values, "fNexttime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeBetween(Date value1, Date value2) {
            addCriterion("f_nexttime between", value1, value2, "fNexttime");
            return (Criteria) this;
        }

        public Criteria andFNexttimeNotBetween(Date value1, Date value2) {
            addCriterion("f_nexttime not between", value1, value2, "fNexttime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}