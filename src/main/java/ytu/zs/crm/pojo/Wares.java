package ytu.zs.crm.pojo;

public class Wares {
    private Integer wid;

    private String wName;

    private Integer wStock;

    private Double wPrice;

    public Integer getWid() {
        return wid;
    }

    public void setWid(Integer wid) {
        this.wid = wid;
    }

    public String getwName() {
        return wName;
    }

    public void setwName(String wName) {
        this.wName = wName == null ? null : wName.trim();
    }

    public Integer getwStock() {
        return wStock;
    }

    public void setwStock(Integer wStock) {
        this.wStock = wStock;
    }

    public Double getwPrice() {
        return wPrice;
    }

    public void setwPrice(Double wPrice) {
        this.wPrice = wPrice;
    }
}
