package ytu.zs.crm.pojo;

import lombok.Data;

/**
 * @ClassName: Echart
 * @Author: zhegsuag
 * @Version: 1.0
 **/
@Data
public class Echart {

    private Integer value;

    private String name;

    public Echart(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Echart() {
    }
}
