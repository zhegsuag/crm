package ytu.zs.crm.pojo;

public class Company {
    private Integer cpid;

    private String cpName;

    private String cpEmail;

    private String cpPhone;

    private String cpAddress;

    private String cpWebsite;

    public Integer getCpid() {
        return cpid;
    }

    public void setCpid(Integer cpid) {
        this.cpid = cpid;
    }

    public String getCpName() {
        return cpName;
    }

    public void setCpName(String cpName) {
        this.cpName = cpName == null ? null : cpName.trim();
    }

    public String getCpEmail() {
        return cpEmail;
    }

    public void setCpEmail(String cpEmail) {
        this.cpEmail = cpEmail == null ? null : cpEmail.trim();
    }

    public String getCpPhone() {
        return cpPhone;
    }

    public void setCpPhone(String cpPhone) {
        this.cpPhone = cpPhone == null ? null : cpPhone.trim();
    }

    public String getCpAddress() {
        return cpAddress;
    }

    public void setCpAddress(String cpAddress) {
        this.cpAddress = cpAddress == null ? null : cpAddress.trim();
    }

    public String getCpWebsite() {
        return cpWebsite;
    }

    public void setCpWebsite(String cpWebsite) {
        this.cpWebsite = cpWebsite == null ? null : cpWebsite.trim();
    }
}
