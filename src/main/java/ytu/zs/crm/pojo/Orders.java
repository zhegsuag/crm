package ytu.zs.crm.pojo;

public class Orders {
    private Integer oid;

    private String wName1;

    private Integer number1;

    private String wName2;

    private Integer number2;

    private String wName3;

    private Integer number3;

    private String wName4;

    private Integer number4;

    private String wName5;

    private Integer number5;

    private String consignee;

    private String address;

    private String phone;

    private String cust;

    private Double total;

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public String getwName1() {
        return wName1;
    }

    public void setwName1(String wName1) {
        this.wName1 = wName1 == null ? null : wName1.trim();
    }

    public Integer getNumber1() {
        return number1;
    }

    public void setNumber1(Integer number1) {
        this.number1 = number1;
    }

    public String getwName2() {
        return wName2;
    }

    public void setwName2(String wName2) {
        this.wName2 = wName2 == null ? null : wName2.trim();
    }

    public Integer getNumber2() {
        return number2;
    }

    public void setNumber2(Integer number2) {
        this.number2 = number2;
    }

    public String getwName3() {
        return wName3;
    }

    public void setwName3(String wName3) {
        this.wName3 = wName3 == null ? null : wName3.trim();
    }

    public Integer getNumber3() {
        return number3;
    }

    public void setNumber3(Integer number3) {
        this.number3 = number3;
    }

    public String getwName4() {
        return wName4;
    }

    public void setwName4(String wName4) {
        this.wName4 = wName4 == null ? null : wName4.trim();
    }

    public Integer getNumber4() {
        return number4;
    }

    public void setNumber4(Integer number4) {
        this.number4 = number4;
    }

    public String getwName5() {
        return wName5;
    }

    public void setwName5(String wName5) {
        this.wName5 = wName5 == null ? null : wName5.trim();
    }

    public Integer getNumber5() {
        return number5;
    }

    public void setNumber5(Integer number5) {
        this.number5 = number5;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee == null ? null : consignee.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust == null ? null : cust.trim();
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}