package ytu.zs.crm.pojo;

import java.util.ArrayList;
import java.util.List;

public class OrdersExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrdersExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOidIsNull() {
            addCriterion("oid is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("oid is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(Integer value) {
            addCriterion("oid =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(Integer value) {
            addCriterion("oid <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(Integer value) {
            addCriterion("oid >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(Integer value) {
            addCriterion("oid >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(Integer value) {
            addCriterion("oid <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(Integer value) {
            addCriterion("oid <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<Integer> values) {
            addCriterion("oid in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<Integer> values) {
            addCriterion("oid not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(Integer value1, Integer value2) {
            addCriterion("oid between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(Integer value1, Integer value2) {
            addCriterion("oid not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andWName1IsNull() {
            addCriterion("w_name1 is null");
            return (Criteria) this;
        }

        public Criteria andWName1IsNotNull() {
            addCriterion("w_name1 is not null");
            return (Criteria) this;
        }

        public Criteria andWName1EqualTo(String value) {
            addCriterion("w_name1 =", value, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1NotEqualTo(String value) {
            addCriterion("w_name1 <>", value, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1GreaterThan(String value) {
            addCriterion("w_name1 >", value, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1GreaterThanOrEqualTo(String value) {
            addCriterion("w_name1 >=", value, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1LessThan(String value) {
            addCriterion("w_name1 <", value, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1LessThanOrEqualTo(String value) {
            addCriterion("w_name1 <=", value, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1Like(String value) {
            addCriterion("w_name1 like", value, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1NotLike(String value) {
            addCriterion("w_name1 not like", value, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1In(List<String> values) {
            addCriterion("w_name1 in", values, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1NotIn(List<String> values) {
            addCriterion("w_name1 not in", values, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1Between(String value1, String value2) {
            addCriterion("w_name1 between", value1, value2, "wName1");
            return (Criteria) this;
        }

        public Criteria andWName1NotBetween(String value1, String value2) {
            addCriterion("w_name1 not between", value1, value2, "wName1");
            return (Criteria) this;
        }

        public Criteria andNumber1IsNull() {
            addCriterion("number1 is null");
            return (Criteria) this;
        }

        public Criteria andNumber1IsNotNull() {
            addCriterion("number1 is not null");
            return (Criteria) this;
        }

        public Criteria andNumber1EqualTo(Integer value) {
            addCriterion("number1 =", value, "number1");
            return (Criteria) this;
        }

        public Criteria andNumber1NotEqualTo(Integer value) {
            addCriterion("number1 <>", value, "number1");
            return (Criteria) this;
        }

        public Criteria andNumber1GreaterThan(Integer value) {
            addCriterion("number1 >", value, "number1");
            return (Criteria) this;
        }

        public Criteria andNumber1GreaterThanOrEqualTo(Integer value) {
            addCriterion("number1 >=", value, "number1");
            return (Criteria) this;
        }

        public Criteria andNumber1LessThan(Integer value) {
            addCriterion("number1 <", value, "number1");
            return (Criteria) this;
        }

        public Criteria andNumber1LessThanOrEqualTo(Integer value) {
            addCriterion("number1 <=", value, "number1");
            return (Criteria) this;
        }

        public Criteria andNumber1In(List<Integer> values) {
            addCriterion("number1 in", values, "number1");
            return (Criteria) this;
        }

        public Criteria andNumber1NotIn(List<Integer> values) {
            addCriterion("number1 not in", values, "number1");
            return (Criteria) this;
        }

        public Criteria andNumber1Between(Integer value1, Integer value2) {
            addCriterion("number1 between", value1, value2, "number1");
            return (Criteria) this;
        }

        public Criteria andNumber1NotBetween(Integer value1, Integer value2) {
            addCriterion("number1 not between", value1, value2, "number1");
            return (Criteria) this;
        }

        public Criteria andWName2IsNull() {
            addCriterion("w_name2 is null");
            return (Criteria) this;
        }

        public Criteria andWName2IsNotNull() {
            addCriterion("w_name2 is not null");
            return (Criteria) this;
        }

        public Criteria andWName2EqualTo(String value) {
            addCriterion("w_name2 =", value, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2NotEqualTo(String value) {
            addCriterion("w_name2 <>", value, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2GreaterThan(String value) {
            addCriterion("w_name2 >", value, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2GreaterThanOrEqualTo(String value) {
            addCriterion("w_name2 >=", value, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2LessThan(String value) {
            addCriterion("w_name2 <", value, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2LessThanOrEqualTo(String value) {
            addCriterion("w_name2 <=", value, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2Like(String value) {
            addCriterion("w_name2 like", value, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2NotLike(String value) {
            addCriterion("w_name2 not like", value, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2In(List<String> values) {
            addCriterion("w_name2 in", values, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2NotIn(List<String> values) {
            addCriterion("w_name2 not in", values, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2Between(String value1, String value2) {
            addCriterion("w_name2 between", value1, value2, "wName2");
            return (Criteria) this;
        }

        public Criteria andWName2NotBetween(String value1, String value2) {
            addCriterion("w_name2 not between", value1, value2, "wName2");
            return (Criteria) this;
        }

        public Criteria andNumber2IsNull() {
            addCriterion("number2 is null");
            return (Criteria) this;
        }

        public Criteria andNumber2IsNotNull() {
            addCriterion("number2 is not null");
            return (Criteria) this;
        }

        public Criteria andNumber2EqualTo(Integer value) {
            addCriterion("number2 =", value, "number2");
            return (Criteria) this;
        }

        public Criteria andNumber2NotEqualTo(Integer value) {
            addCriterion("number2 <>", value, "number2");
            return (Criteria) this;
        }

        public Criteria andNumber2GreaterThan(Integer value) {
            addCriterion("number2 >", value, "number2");
            return (Criteria) this;
        }

        public Criteria andNumber2GreaterThanOrEqualTo(Integer value) {
            addCriterion("number2 >=", value, "number2");
            return (Criteria) this;
        }

        public Criteria andNumber2LessThan(Integer value) {
            addCriterion("number2 <", value, "number2");
            return (Criteria) this;
        }

        public Criteria andNumber2LessThanOrEqualTo(Integer value) {
            addCriterion("number2 <=", value, "number2");
            return (Criteria) this;
        }

        public Criteria andNumber2In(List<Integer> values) {
            addCriterion("number2 in", values, "number2");
            return (Criteria) this;
        }

        public Criteria andNumber2NotIn(List<Integer> values) {
            addCriterion("number2 not in", values, "number2");
            return (Criteria) this;
        }

        public Criteria andNumber2Between(Integer value1, Integer value2) {
            addCriterion("number2 between", value1, value2, "number2");
            return (Criteria) this;
        }

        public Criteria andNumber2NotBetween(Integer value1, Integer value2) {
            addCriterion("number2 not between", value1, value2, "number2");
            return (Criteria) this;
        }

        public Criteria andWName3IsNull() {
            addCriterion("w_name3 is null");
            return (Criteria) this;
        }

        public Criteria andWName3IsNotNull() {
            addCriterion("w_name3 is not null");
            return (Criteria) this;
        }

        public Criteria andWName3EqualTo(String value) {
            addCriterion("w_name3 =", value, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3NotEqualTo(String value) {
            addCriterion("w_name3 <>", value, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3GreaterThan(String value) {
            addCriterion("w_name3 >", value, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3GreaterThanOrEqualTo(String value) {
            addCriterion("w_name3 >=", value, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3LessThan(String value) {
            addCriterion("w_name3 <", value, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3LessThanOrEqualTo(String value) {
            addCriterion("w_name3 <=", value, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3Like(String value) {
            addCriterion("w_name3 like", value, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3NotLike(String value) {
            addCriterion("w_name3 not like", value, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3In(List<String> values) {
            addCriterion("w_name3 in", values, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3NotIn(List<String> values) {
            addCriterion("w_name3 not in", values, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3Between(String value1, String value2) {
            addCriterion("w_name3 between", value1, value2, "wName3");
            return (Criteria) this;
        }

        public Criteria andWName3NotBetween(String value1, String value2) {
            addCriterion("w_name3 not between", value1, value2, "wName3");
            return (Criteria) this;
        }

        public Criteria andNumber3IsNull() {
            addCriterion("number3 is null");
            return (Criteria) this;
        }

        public Criteria andNumber3IsNotNull() {
            addCriterion("number3 is not null");
            return (Criteria) this;
        }

        public Criteria andNumber3EqualTo(Integer value) {
            addCriterion("number3 =", value, "number3");
            return (Criteria) this;
        }

        public Criteria andNumber3NotEqualTo(Integer value) {
            addCriterion("number3 <>", value, "number3");
            return (Criteria) this;
        }

        public Criteria andNumber3GreaterThan(Integer value) {
            addCriterion("number3 >", value, "number3");
            return (Criteria) this;
        }

        public Criteria andNumber3GreaterThanOrEqualTo(Integer value) {
            addCriterion("number3 >=", value, "number3");
            return (Criteria) this;
        }

        public Criteria andNumber3LessThan(Integer value) {
            addCriterion("number3 <", value, "number3");
            return (Criteria) this;
        }

        public Criteria andNumber3LessThanOrEqualTo(Integer value) {
            addCriterion("number3 <=", value, "number3");
            return (Criteria) this;
        }

        public Criteria andNumber3In(List<Integer> values) {
            addCriterion("number3 in", values, "number3");
            return (Criteria) this;
        }

        public Criteria andNumber3NotIn(List<Integer> values) {
            addCriterion("number3 not in", values, "number3");
            return (Criteria) this;
        }

        public Criteria andNumber3Between(Integer value1, Integer value2) {
            addCriterion("number3 between", value1, value2, "number3");
            return (Criteria) this;
        }

        public Criteria andNumber3NotBetween(Integer value1, Integer value2) {
            addCriterion("number3 not between", value1, value2, "number3");
            return (Criteria) this;
        }

        public Criteria andWName4IsNull() {
            addCriterion("w_name4 is null");
            return (Criteria) this;
        }

        public Criteria andWName4IsNotNull() {
            addCriterion("w_name4 is not null");
            return (Criteria) this;
        }

        public Criteria andWName4EqualTo(String value) {
            addCriterion("w_name4 =", value, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4NotEqualTo(String value) {
            addCriterion("w_name4 <>", value, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4GreaterThan(String value) {
            addCriterion("w_name4 >", value, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4GreaterThanOrEqualTo(String value) {
            addCriterion("w_name4 >=", value, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4LessThan(String value) {
            addCriterion("w_name4 <", value, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4LessThanOrEqualTo(String value) {
            addCriterion("w_name4 <=", value, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4Like(String value) {
            addCriterion("w_name4 like", value, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4NotLike(String value) {
            addCriterion("w_name4 not like", value, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4In(List<String> values) {
            addCriterion("w_name4 in", values, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4NotIn(List<String> values) {
            addCriterion("w_name4 not in", values, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4Between(String value1, String value2) {
            addCriterion("w_name4 between", value1, value2, "wName4");
            return (Criteria) this;
        }

        public Criteria andWName4NotBetween(String value1, String value2) {
            addCriterion("w_name4 not between", value1, value2, "wName4");
            return (Criteria) this;
        }

        public Criteria andNumber4IsNull() {
            addCriterion("number4 is null");
            return (Criteria) this;
        }

        public Criteria andNumber4IsNotNull() {
            addCriterion("number4 is not null");
            return (Criteria) this;
        }

        public Criteria andNumber4EqualTo(Integer value) {
            addCriterion("number4 =", value, "number4");
            return (Criteria) this;
        }

        public Criteria andNumber4NotEqualTo(Integer value) {
            addCriterion("number4 <>", value, "number4");
            return (Criteria) this;
        }

        public Criteria andNumber4GreaterThan(Integer value) {
            addCriterion("number4 >", value, "number4");
            return (Criteria) this;
        }

        public Criteria andNumber4GreaterThanOrEqualTo(Integer value) {
            addCriterion("number4 >=", value, "number4");
            return (Criteria) this;
        }

        public Criteria andNumber4LessThan(Integer value) {
            addCriterion("number4 <", value, "number4");
            return (Criteria) this;
        }

        public Criteria andNumber4LessThanOrEqualTo(Integer value) {
            addCriterion("number4 <=", value, "number4");
            return (Criteria) this;
        }

        public Criteria andNumber4In(List<Integer> values) {
            addCriterion("number4 in", values, "number4");
            return (Criteria) this;
        }

        public Criteria andNumber4NotIn(List<Integer> values) {
            addCriterion("number4 not in", values, "number4");
            return (Criteria) this;
        }

        public Criteria andNumber4Between(Integer value1, Integer value2) {
            addCriterion("number4 between", value1, value2, "number4");
            return (Criteria) this;
        }

        public Criteria andNumber4NotBetween(Integer value1, Integer value2) {
            addCriterion("number4 not between", value1, value2, "number4");
            return (Criteria) this;
        }

        public Criteria andWName5IsNull() {
            addCriterion("w_name5 is null");
            return (Criteria) this;
        }

        public Criteria andWName5IsNotNull() {
            addCriterion("w_name5 is not null");
            return (Criteria) this;
        }

        public Criteria andWName5EqualTo(String value) {
            addCriterion("w_name5 =", value, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5NotEqualTo(String value) {
            addCriterion("w_name5 <>", value, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5GreaterThan(String value) {
            addCriterion("w_name5 >", value, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5GreaterThanOrEqualTo(String value) {
            addCriterion("w_name5 >=", value, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5LessThan(String value) {
            addCriterion("w_name5 <", value, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5LessThanOrEqualTo(String value) {
            addCriterion("w_name5 <=", value, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5Like(String value) {
            addCriterion("w_name5 like", value, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5NotLike(String value) {
            addCriterion("w_name5 not like", value, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5In(List<String> values) {
            addCriterion("w_name5 in", values, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5NotIn(List<String> values) {
            addCriterion("w_name5 not in", values, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5Between(String value1, String value2) {
            addCriterion("w_name5 between", value1, value2, "wName5");
            return (Criteria) this;
        }

        public Criteria andWName5NotBetween(String value1, String value2) {
            addCriterion("w_name5 not between", value1, value2, "wName5");
            return (Criteria) this;
        }

        public Criteria andNumber5IsNull() {
            addCriterion("number5 is null");
            return (Criteria) this;
        }

        public Criteria andNumber5IsNotNull() {
            addCriterion("number5 is not null");
            return (Criteria) this;
        }

        public Criteria andNumber5EqualTo(Integer value) {
            addCriterion("number5 =", value, "number5");
            return (Criteria) this;
        }

        public Criteria andNumber5NotEqualTo(Integer value) {
            addCriterion("number5 <>", value, "number5");
            return (Criteria) this;
        }

        public Criteria andNumber5GreaterThan(Integer value) {
            addCriterion("number5 >", value, "number5");
            return (Criteria) this;
        }

        public Criteria andNumber5GreaterThanOrEqualTo(Integer value) {
            addCriterion("number5 >=", value, "number5");
            return (Criteria) this;
        }

        public Criteria andNumber5LessThan(Integer value) {
            addCriterion("number5 <", value, "number5");
            return (Criteria) this;
        }

        public Criteria andNumber5LessThanOrEqualTo(Integer value) {
            addCriterion("number5 <=", value, "number5");
            return (Criteria) this;
        }

        public Criteria andNumber5In(List<Integer> values) {
            addCriterion("number5 in", values, "number5");
            return (Criteria) this;
        }

        public Criteria andNumber5NotIn(List<Integer> values) {
            addCriterion("number5 not in", values, "number5");
            return (Criteria) this;
        }

        public Criteria andNumber5Between(Integer value1, Integer value2) {
            addCriterion("number5 between", value1, value2, "number5");
            return (Criteria) this;
        }

        public Criteria andNumber5NotBetween(Integer value1, Integer value2) {
            addCriterion("number5 not between", value1, value2, "number5");
            return (Criteria) this;
        }

        public Criteria andConsigneeIsNull() {
            addCriterion("consignee is null");
            return (Criteria) this;
        }

        public Criteria andConsigneeIsNotNull() {
            addCriterion("consignee is not null");
            return (Criteria) this;
        }

        public Criteria andConsigneeEqualTo(String value) {
            addCriterion("consignee =", value, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeNotEqualTo(String value) {
            addCriterion("consignee <>", value, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeGreaterThan(String value) {
            addCriterion("consignee >", value, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeGreaterThanOrEqualTo(String value) {
            addCriterion("consignee >=", value, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeLessThan(String value) {
            addCriterion("consignee <", value, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeLessThanOrEqualTo(String value) {
            addCriterion("consignee <=", value, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeLike(String value) {
            addCriterion("consignee like", value, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeNotLike(String value) {
            addCriterion("consignee not like", value, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeIn(List<String> values) {
            addCriterion("consignee in", values, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeNotIn(List<String> values) {
            addCriterion("consignee not in", values, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeBetween(String value1, String value2) {
            addCriterion("consignee between", value1, value2, "consignee");
            return (Criteria) this;
        }

        public Criteria andConsigneeNotBetween(String value1, String value2) {
            addCriterion("consignee not between", value1, value2, "consignee");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andCustIsNull() {
            addCriterion("cust is null");
            return (Criteria) this;
        }

        public Criteria andCustIsNotNull() {
            addCriterion("cust is not null");
            return (Criteria) this;
        }

        public Criteria andCustEqualTo(String value) {
            addCriterion("cust =", value, "cust");
            return (Criteria) this;
        }

        public Criteria andCustNotEqualTo(String value) {
            addCriterion("cust <>", value, "cust");
            return (Criteria) this;
        }

        public Criteria andCustGreaterThan(String value) {
            addCriterion("cust >", value, "cust");
            return (Criteria) this;
        }

        public Criteria andCustGreaterThanOrEqualTo(String value) {
            addCriterion("cust >=", value, "cust");
            return (Criteria) this;
        }

        public Criteria andCustLessThan(String value) {
            addCriterion("cust <", value, "cust");
            return (Criteria) this;
        }

        public Criteria andCustLessThanOrEqualTo(String value) {
            addCriterion("cust <=", value, "cust");
            return (Criteria) this;
        }

        public Criteria andCustLike(String value) {
            addCriterion("cust like", value, "cust");
            return (Criteria) this;
        }

        public Criteria andCustNotLike(String value) {
            addCriterion("cust not like", value, "cust");
            return (Criteria) this;
        }

        public Criteria andCustIn(List<String> values) {
            addCriterion("cust in", values, "cust");
            return (Criteria) this;
        }

        public Criteria andCustNotIn(List<String> values) {
            addCriterion("cust not in", values, "cust");
            return (Criteria) this;
        }

        public Criteria andCustBetween(String value1, String value2) {
            addCriterion("cust between", value1, value2, "cust");
            return (Criteria) this;
        }

        public Criteria andCustNotBetween(String value1, String value2) {
            addCriterion("cust not between", value1, value2, "cust");
            return (Criteria) this;
        }

        public Criteria andTotalIsNull() {
            addCriterion("total is null");
            return (Criteria) this;
        }

        public Criteria andTotalIsNotNull() {
            addCriterion("total is not null");
            return (Criteria) this;
        }

        public Criteria andTotalEqualTo(Double value) {
            addCriterion("total =", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotEqualTo(Double value) {
            addCriterion("total <>", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalGreaterThan(Double value) {
            addCriterion("total >", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalGreaterThanOrEqualTo(Double value) {
            addCriterion("total >=", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLessThan(Double value) {
            addCriterion("total <", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLessThanOrEqualTo(Double value) {
            addCriterion("total <=", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalIn(List<Double> values) {
            addCriterion("total in", values, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotIn(List<Double> values) {
            addCriterion("total not in", values, "total");
            return (Criteria) this;
        }

        public Criteria andTotalBetween(Double value1, Double value2) {
            addCriterion("total between", value1, value2, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotBetween(Double value1, Double value2) {
            addCriterion("total not between", value1, value2, "total");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}