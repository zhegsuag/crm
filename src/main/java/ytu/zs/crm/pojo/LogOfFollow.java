package ytu.zs.crm.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.Date;
@Data
public class LogOfFollow {
    private Integer lfId;

    private Integer cid;

    private String lfContact;

    private String lfState;

    private String lfCommunicate;

    @JsonFormat(locale = "zh",timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lfDatatime;

    @JsonFormat(locale = "zh",timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lfNexttime;

    private String cName;

    private String cSex;

    private Integer cAge;

    private String cPhone;

    private String cEmail;

    private String cAddress;

    private String cpName;

    private Date cBirth;

    private String cUserName;
}
