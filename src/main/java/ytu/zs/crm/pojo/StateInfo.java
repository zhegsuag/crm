package ytu.zs.crm.pojo;

import java.io.Serializable;

public class StateInfo implements Serializable,Comparable {
    private Integer sid;

    private Integer snum;

    private String state;

    private String key;

    private String value;

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getSnum() {
        return snum;
    }

    public void setSnum(Integer snum) {
        this.snum = snum;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public int compareTo(Object o){
        if(o instanceof StateInfo){
            return snum.compareTo(((StateInfo) o).getSnum());
        }
        return 1;
    }
}
