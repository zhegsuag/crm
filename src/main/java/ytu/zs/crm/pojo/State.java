package ytu.zs.crm.pojo;

public class State {
    @Override
    public String toString() {
        return "危险客户：" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ordnum=" + ordnum +
                ", state='" + state + '\'';
    }

    private Integer id;

    private String name;

    private Integer ordnum;

    private String state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getOrdnum() {
        return ordnum;
    }

    public void setOrdnum(Integer ordnum) {
        this.ordnum = ordnum;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }
}
