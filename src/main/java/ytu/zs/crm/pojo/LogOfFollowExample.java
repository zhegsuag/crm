package ytu.zs.crm.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LogOfFollowExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public LogOfFollowExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLfIdIsNull() {
            addCriterion("lf_id is null");
            return (Criteria) this;
        }

        public Criteria andLfIdIsNotNull() {
            addCriterion("lf_id is not null");
            return (Criteria) this;
        }

        public Criteria andLfIdEqualTo(Integer value) {
            addCriterion("lf_id =", value, "lfId");
            return (Criteria) this;
        }

        public Criteria andLfIdNotEqualTo(Integer value) {
            addCriterion("lf_id <>", value, "lfId");
            return (Criteria) this;
        }

        public Criteria andLfIdGreaterThan(Integer value) {
            addCriterion("lf_id >", value, "lfId");
            return (Criteria) this;
        }

        public Criteria andLfIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("lf_id >=", value, "lfId");
            return (Criteria) this;
        }

        public Criteria andLfIdLessThan(Integer value) {
            addCriterion("lf_id <", value, "lfId");
            return (Criteria) this;
        }

        public Criteria andLfIdLessThanOrEqualTo(Integer value) {
            addCriterion("lf_id <=", value, "lfId");
            return (Criteria) this;
        }

        public Criteria andLfIdIn(List<Integer> values) {
            addCriterion("lf_id in", values, "lfId");
            return (Criteria) this;
        }

        public Criteria andLfIdNotIn(List<Integer> values) {
            addCriterion("lf_id not in", values, "lfId");
            return (Criteria) this;
        }

        public Criteria andLfIdBetween(Integer value1, Integer value2) {
            addCriterion("lf_id between", value1, value2, "lfId");
            return (Criteria) this;
        }

        public Criteria andLfIdNotBetween(Integer value1, Integer value2) {
            addCriterion("lf_id not between", value1, value2, "lfId");
            return (Criteria) this;
        }

        public Criteria andCidIsNull() {
            addCriterion("cid is null");
            return (Criteria) this;
        }

        public Criteria andCidIsNotNull() {
            addCriterion("cid is not null");
            return (Criteria) this;
        }

        public Criteria andCidEqualTo(Integer value) {
            addCriterion("cid =", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotEqualTo(Integer value) {
            addCriterion("cid <>", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThan(Integer value) {
            addCriterion("cid >", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThanOrEqualTo(Integer value) {
            addCriterion("cid >=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThan(Integer value) {
            addCriterion("cid <", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThanOrEqualTo(Integer value) {
            addCriterion("cid <=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidIn(List<Integer> values) {
            addCriterion("cid in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotIn(List<Integer> values) {
            addCriterion("cid not in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidBetween(Integer value1, Integer value2) {
            addCriterion("cid between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotBetween(Integer value1, Integer value2) {
            addCriterion("cid not between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andLfContactIsNull() {
            addCriterion("lf_contact is null");
            return (Criteria) this;
        }

        public Criteria andLfContactIsNotNull() {
            addCriterion("lf_contact is not null");
            return (Criteria) this;
        }

        public Criteria andLfContactEqualTo(String value) {
            addCriterion("lf_contact =", value, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactNotEqualTo(String value) {
            addCriterion("lf_contact <>", value, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactGreaterThan(String value) {
            addCriterion("lf_contact >", value, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactGreaterThanOrEqualTo(String value) {
            addCriterion("lf_contact >=", value, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactLessThan(String value) {
            addCriterion("lf_contact <", value, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactLessThanOrEqualTo(String value) {
            addCriterion("lf_contact <=", value, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactLike(String value) {
            addCriterion("lf_contact like", value, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactNotLike(String value) {
            addCriterion("lf_contact not like", value, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactIn(List<String> values) {
            addCriterion("lf_contact in", values, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactNotIn(List<String> values) {
            addCriterion("lf_contact not in", values, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactBetween(String value1, String value2) {
            addCriterion("lf_contact between", value1, value2, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfContactNotBetween(String value1, String value2) {
            addCriterion("lf_contact not between", value1, value2, "lfContact");
            return (Criteria) this;
        }

        public Criteria andLfStateIsNull() {
            addCriterion("lf_state is null");
            return (Criteria) this;
        }

        public Criteria andLfStateIsNotNull() {
            addCriterion("lf_state is not null");
            return (Criteria) this;
        }

        public Criteria andLfStateEqualTo(String value) {
            addCriterion("lf_state =", value, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateNotEqualTo(String value) {
            addCriterion("lf_state <>", value, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateGreaterThan(String value) {
            addCriterion("lf_state >", value, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateGreaterThanOrEqualTo(String value) {
            addCriterion("lf_state >=", value, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateLessThan(String value) {
            addCriterion("lf_state <", value, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateLessThanOrEqualTo(String value) {
            addCriterion("lf_state <=", value, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateLike(String value) {
            addCriterion("lf_state like", value, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateNotLike(String value) {
            addCriterion("lf_state not like", value, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateIn(List<String> values) {
            addCriterion("lf_state in", values, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateNotIn(List<String> values) {
            addCriterion("lf_state not in", values, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateBetween(String value1, String value2) {
            addCriterion("lf_state between", value1, value2, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfStateNotBetween(String value1, String value2) {
            addCriterion("lf_state not between", value1, value2, "lfState");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateIsNull() {
            addCriterion("lf_communicate is null");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateIsNotNull() {
            addCriterion("lf_communicate is not null");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateEqualTo(String value) {
            addCriterion("lf_communicate =", value, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateNotEqualTo(String value) {
            addCriterion("lf_communicate <>", value, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateGreaterThan(String value) {
            addCriterion("lf_communicate >", value, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateGreaterThanOrEqualTo(String value) {
            addCriterion("lf_communicate >=", value, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateLessThan(String value) {
            addCriterion("lf_communicate <", value, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateLessThanOrEqualTo(String value) {
            addCriterion("lf_communicate <=", value, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateLike(String value) {
            addCriterion("lf_communicate like", value, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateNotLike(String value) {
            addCriterion("lf_communicate not like", value, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateIn(List<String> values) {
            addCriterion("lf_communicate in", values, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateNotIn(List<String> values) {
            addCriterion("lf_communicate not in", values, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateBetween(String value1, String value2) {
            addCriterion("lf_communicate between", value1, value2, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfCommunicateNotBetween(String value1, String value2) {
            addCriterion("lf_communicate not between", value1, value2, "lfCommunicate");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeIsNull() {
            addCriterion("lf_datatime is null");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeIsNotNull() {
            addCriterion("lf_datatime is not null");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeEqualTo(Date value) {
            addCriterion("lf_datatime =", value, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeNotEqualTo(Date value) {
            addCriterion("lf_datatime <>", value, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeGreaterThan(Date value) {
            addCriterion("lf_datatime >", value, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeGreaterThanOrEqualTo(Date value) {
            addCriterion("lf_datatime >=", value, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeLessThan(Date value) {
            addCriterion("lf_datatime <", value, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeLessThanOrEqualTo(Date value) {
            addCriterion("lf_datatime <=", value, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeIn(List<Date> values) {
            addCriterion("lf_datatime in", values, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeNotIn(List<Date> values) {
            addCriterion("lf_datatime not in", values, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeBetween(Date value1, Date value2) {
            addCriterion("lf_datatime between", value1, value2, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfDatatimeNotBetween(Date value1, Date value2) {
            addCriterion("lf_datatime not between", value1, value2, "lfDatatime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeIsNull() {
            addCriterion("lf_nexttime is null");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeIsNotNull() {
            addCriterion("lf_nexttime is not null");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeEqualTo(Date value) {
            addCriterion("lf_nexttime =", value, "lfNexttime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeNotEqualTo(Date value) {
            addCriterion("lf_nexttime <>", value, "lfNexttime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeGreaterThan(Date value) {
            addCriterion("lf_nexttime >", value, "lfNexttime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("lf_nexttime >=", value, "lfNexttime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeLessThan(Date value) {
            addCriterion("lf_nexttime <", value, "lfNexttime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeLessThanOrEqualTo(Date value) {
            addCriterion("lf_nexttime <=", value, "lfNexttime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeIn(List<Date> values) {
            addCriterion("lf_nexttime in", values, "lfNexttime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeNotIn(List<Date> values) {
            addCriterion("lf_nexttime not in", values, "lfNexttime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeBetween(Date value1, Date value2) {
            addCriterion("lf_nexttime between", value1, value2, "lfNexttime");
            return (Criteria) this;
        }

        public Criteria andLfNexttimeNotBetween(Date value1, Date value2) {
            addCriterion("lf_nexttime not between", value1, value2, "lfNexttime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}