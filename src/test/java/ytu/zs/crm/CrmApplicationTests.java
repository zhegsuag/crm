package ytu.zs.crm;

import ytu.zs.crm.pojo.StateInfo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class CrmApplicationTests {

    @Test
    public int test(){
        int number = 3;
        List<StateInfo> intarray = new ArrayList<>();
        StateInfo stateInfo = new StateInfo();
        stateInfo.setSnum(0);
        stateInfo.setState("sdvsv");
        intarray.add(stateInfo);
        stateInfo.setSnum(5);
        stateInfo.setState("123456");
        intarray.add(stateInfo);
        int index = Math.abs(number-intarray.get(0).getSnum());
        int result = intarray.get(0).getSnum();
        for (StateInfo i : intarray) {
            int abs = Math.abs(number-i.getSnum());
            if(abs <= index){
                index = abs;
                result = i.getSnum();
            }
        }
        return result;
    }

}
